<?php
namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;
use App\Exports\RankingExport;
use Maatwebsite\Excel\Facades\Excel;


class ExportRankingAction extends AbstractAction {
    public function getTitle() {
        return __('app.export_ranking');
    }

    public function getIcon() {
        return 'voyager-bar-chart';
    }

    public function getPolicy() {
        return 'read';
    }

    public function getAttributes() {
        return [
            'class' => 'd-none ',
            'hidden' => 'hidden',
        ];
    }

    public function getDefaultRoute() {
        return route('admin.grupos');
    }

    public function shouldActionDisplayOnDataType() {
        return $this->dataType->slug == 'grupos';
    }

    public function massAction($ids, $comingFrom) {
       // return Excel::download(new RankingExport, 'report.xlsx');
        return $ids;
    }
}
