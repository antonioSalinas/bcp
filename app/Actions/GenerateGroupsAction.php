<?php

namespace App\Actions;

use App\Grupo;
use App\User;
use TCG\Voyager\Actions\AbstractAction;


class GenerateGroupsAction extends AbstractAction {
    public function getTitle() {
        return __('app.generate_groups');
    }

    public function getIcon() {
        return 'voyager-tools';
    }

    public function getPolicy() {
        return 'read';
    }

    public function getAttributes() {
        return [
            'class' => 'btn btn-sm btn-primary',
        ];
    }

    public function getDefaultRoute() {
        return route('admin.grupos');
    }

    public function shouldActionDisplayOnDataType() {
        return $this->dataType->slug == 'grupos';
    }

    public function massAction($ids, $comingFrom) {
        $freeUsers = User::freePlayers()->inRandomOrder()->get()->all();

        $players = [];
        $count = 1;
        $qtyPlayerPerGroup = 3;

        foreach ($freeUsers as $ind => $user) {
            $players[] = $user;
            if((($ind+1) % $qtyPlayerPerGroup) === 0) {
                $group = new Grupo();
                $group->nombre = 'Grupo ' . $count;
                $group->habilitado = 1;
                $group->save();

                $group->users()->saveMany($players);

                $players = [];
                $count++;
            }
        }

        if(!empty($players)) {
            $group = new Grupo();
            $group->nombre = 'Grupo ' . $count;
            $group->habilitado = 1;
            $group->save();

            $group->users()->saveMany($players);
        }

        return redirect($comingFrom);
    }
}
