<?php
namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class SendInformationMailAction extends AbstractAction {
    public function getTitle()
    {
        return __('app.send_information');
    }

    public function getIcon()
    {
        return 'voyager-mail';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary pull-right edit',
        ];
    }

    public function getDefaultRoute()
    {
        return route('admin.grupos', array("grupoId"=>$this->data->{$this->data->getKeyName()}));
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'grupos';
    }
}
