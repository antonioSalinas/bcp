<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Equipo extends Model
{
    protected $fillable = [
        'descripcion'
    ];
    // use SoftDeletes;
    protected $dates = ['deleted_at'];
}
