<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
use App\User;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ColumnasExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        $fields = DB::select("SELECT `COLUMN_NAME` 
        FROM `INFORMATION_SCHEMA`.`COLUMNS` 
        WHERE `TABLE_SCHEMA`='acertijosdb2' 
            AND `TABLE_NAME`='users';");
            //die (json_encode($fields));
        $transpond= [];
        $noShow=['id','avatar','api_token',	'remember_token',	'settings',	'created_at',	'updated_at','email_verified_at'	];

        foreach($fields as $field){
            if(! in_array($field->COLUMN_NAME,$noShow))
            {
                $transpond[] =    $field->COLUMN_NAME;
            }
            
        }
       // die (json_encode($transpond));

        return $transpond;
    }

    public function collection()
    {
      
        
        
        //die (json_encode($transpond));
        return collect();
    }
}
