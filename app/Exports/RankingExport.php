<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\Sheets\ReportGroupsSheet;
use App\Exports\Sheets\ReportUserSheet;


class RankingExport implements WithMultipleSheets {
    use Exportable;
    protected $filter;
    public function __construct(array $filter)
    {
        $this->filter = $filter;
    }
    public function sheets(): array
    {
        $sheets = [
            new ReportGroupsSheet($this->filter),
            new ReportUserSheet($this->filter)
        ];


        return $sheets;
    }
}
