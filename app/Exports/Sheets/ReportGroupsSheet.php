<?php
namespace App\Exports\Sheets;

use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;


class ReportGroupsSheet implements FromView, WithTitle {
    protected $filter;
    protected $arrFilter;
    public function __construct(array $filter)
    {
        $this->filter = $filter;
        $arrFilter = [];
        foreach($this->filter as $fill){
          $arrFilter[] = $fill['id'];
        }
        $this->arrFilter = $arrFilter;
    }

    public function view(): View {


        $grupos = DB::table('grupos', 'g', '')
            ->leftJoin(DB::raw('(SELECT * FROM `grupo_acertijos` ORDER BY `acertijo_id`) as ga '), function ($join) {
                $join->on('g.id', '=', 'ga.grupo_id');
            })->whereIn("grupo_id",$this->arrFilter)
            ->select(
                'g.nombre',
                'g.created_at',
//                DB::raw('ROW_NUMBER() OVER(ORDER BY g.id DESC) AS position'),
                DB::raw('ifnull(sum(ga.puntuacion),0) as puntos'),
                DB::raw("ifnull(sum(TIMESTAMPDIFF(SECOND, ga.tiempo_inicio,  ga.tiempo_final)), 999999999) as tiempo"),
                DB::raw('group_concat(ga.puntuacion, "/", TIMESTAMPDIFF(SECOND, ga.tiempo_inicio, ga.tiempo_final)) as acertijos')
            )
            ->orderBy('puntos', 'desc')
            ->orderBy('tiempo', 'asc')
            ->orderBy('g.nombre', 'asc')
            ->groupBy('g.id')
            ->get();

        return view('exports.report-groups', [
            'grupos' => $grupos
        ]);
    }

    public function title(): string
    {
        return 'Informe de Grupo';
    }
}
