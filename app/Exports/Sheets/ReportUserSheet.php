<?php
namespace App\Exports\Sheets;

use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;
use App\User;


class ReportUserSheet implements FromView, WithTitle {
    protected $filter;
    protected $arrFilter;
    public function __construct(array $filter)
    {
        $this->filter = $filter;
        $arrFilter = [];
        foreach($filter as $fill){
            $arrFilter[] = $fill['id'];
        }
        $this->arrFilter = $arrFilter;
    }
    public function view(): View {

        $users = User::where('role_id', '<>', '1')->whereIn('grupo_id',$this->arrFilter)->get();

        return view('exports.report-users', [
            'users' => $users
        ]);
    }

    public function title(): string
    {
        return 'Informe de usuario';
    }

}
