<?php

namespace App;

use App\Events\SendInformacionMailEvent;
use Illuminate\Database\Eloquent\Model;
use App\InformacionUserGrupo;
use App\UserGrupo;
use App\GrupoAcertijo;
use Illuminate\Database\Eloquent\SoftDeletes;


class Grupo extends Model {
   // use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'nombre','habilitado','equipo_id'
    ];
    protected static function boot() {
        parent::boot();
        Grupo::deleting(function ($model) {
            $user_grupos = UserGrupo::where('grupo_id', $model->id)->get()->all();
            foreach ($user_grupos as $user_grupo) {
                InformacionUserGrupo::where('user_grupo_id', $user_grupo->id)->delete();
                $user_grupo->delete();
            }

            GrupoAcertijo::where('grupo_id', $model->id)->delete();
        });
    }

    public function scopeEnabled($query) {
        return $query->where('enabled', 1);
    }

    public function getHabilitadoBrowseAttribute() {
        return $this->habilitado ? __('voyager::generic.yes') : __('voyager::generic.no');
    }

    public function getHabilitadoReadAttribute() {
        return $this->habilitado ? __('voyager::generic.yes') : __('voyager::generic.no');
    }

    public function crearGrupoAcertijo() {
        $grupoAcertijo =  $this->hasMany('App\GrupoAcertijo', 'grupo_id')
            ->orderBy('id','desc')
            ->first();

        $acertijo_id = 0;
        if(empty($grupoAcertijo)) {
            $count = Acertijo::count();
            if($count === 0)
                return null;
        } else
            $acertijo_id = $grupoAcertijo->acertijo_id;

        $acertijo = Acertijo::orderBy('orden', 'asc')
            ->where('id', '>', $acertijo_id)
            ->where('activo', 1)
            ->first();

        if(empty($acertijo))
            return null;

        $grupoAcertijo = new GrupoAcertijo();
        $grupoAcertijo->grupo_id = $this->id;
        $grupoAcertijo->acertijo_id = $acertijo->id;
        $grupoAcertijo->puntuacion = 0;
        $grupoAcertijo->pistas_usadas = 0;
//        $grupoAcertijo->tiempo_inicio = Date::now();
        $grupoAcertijo->tiempo_inicio = null;
        $grupoAcertijo->save();

        return $grupoAcertijo;
    }

    public function ultimoGrupoAcertijo() {
        $grupoAcertijo = $this->hasMany('App\GrupoAcertijo', 'grupo_id')
            ->orderBy('id','desc')
            ->first();

        if(empty($grupoAcertijo)) {
            $grupoAcertijo = $this->crearGrupoAcertijo();
        }
        return $grupoAcertijo;
    }

    public function ultimoAcertijo() {
        return $this->belongsToMany('App\Acertijo', 'grupo_acertijos')
            ->orderBy('id','desc')
            ->first();
    }

    public function users() {
        return $this->belongsToMany('App\User', 'user_grupos');
    }

    public function userGrupos() {
        return $this->hasMany('App\UserGrupo', 'grupo_id');
    }

    public function generarInformacion($acertijo_id){
        $user_grupos = $this->userGrupos()->get()->all();

        $informaciones = Informacione::where('acertijo_id', $acertijo_id)
            ->get()
            ->all();

        //Obteniendo todos las informaciones entregadas a los usuarios del grupo
//        $informaciones_entregadas = [];
        $informacion_user_grupos = [];
        foreach($user_grupos as $userGrupo) {
            $temp = $userGrupo->informacionUserGrupos()->get()->all();
            $informacion_user_grupos = array_merge($informacion_user_grupos, $temp);
        }

        //Filtrando por $acertijo_id
//        $informaciones_entregadas = array_filter($informaciones_entregadas, function ($informaciones_entregada) use ($acertijo_id) {
        $informacion_user_grupos = array_filter($informacion_user_grupos, function ($informacion_user_grupo) use ($acertijo_id) {
            if ((isset($informacion_user_grupo->informacion->acertijo_id)) && (isset($acertijo_id))){
            return $informacion_user_grupo->informacion->acertijo_id === $acertijo_id;
            }else{return null;}
        });

//        $informaciones_libres = array_filter($informaciones, function ($informacion) use ($informaciones_entregadas) {
        $informaciones_libres = array_filter($informaciones, function ($informacion) use ($informacion_user_grupos) {
//            foreach ($informaciones_entregadas as $informacion_entregada) {
            foreach ($informacion_user_grupos as $informacion_user_grupo) {
//                if($informacion_entregada->informacion_id === $informacion->id)
                if($informacion_user_grupo->informacion_id === $informacion->id)
                    return false;
            }
            return true;
        });

//        $user_grupo_libres = array_filter($user_grupos, function ($userGrupo) use ($informaciones_entregadas) {
        $user_grupo_libres = array_filter($user_grupos, function ($userGrupo) use ($informacion_user_grupos) {
//            foreach ($informaciones_entregadas as $informacion_entregada) {
            foreach ($informacion_user_grupos as $informacion_user_grupo) {
//                if($informacion_entregada->user_grupo_id === $userGrupo->id)
                if($informacion_user_grupo->user_grupo_id === $userGrupo->id)
                    return false;
            }
            return true;
        });

        // Repartiendo informacion a usuarios del grupo
        if(count($user_grupo_libres) === 0)
            return;

        $div = floor(count($informaciones_libres) / count($user_grupo_libres));
        $res = count($informaciones_libres) % count($user_grupo_libres);
        $count = 0;
        for($i = 0; $i < $div; $i++) {
            foreach($user_grupo_libres as $user_grupo) {
                $informacionUserGrupo = new InformacionUserGrupo();
                $informacionUserGrupo->user_grupo_id = $user_grupo->id;
                $informacionUserGrupo->informacion_id = $informaciones_libres[$count]->id;
                $informacionUserGrupo->save();
                $count++;
            }
        }

        for($i = $count; $i < count($informaciones_libres); $i++) {
            for($j = 0; $j < $res; $j++) {
                $informacionUserGrupo = new InformacionUserGrupo();
                $informacionUserGrupo->user_grupo_id = $user_grupo_libres[$j]->id;
                $informacionUserGrupo->informacion_id = $informaciones_libres[$i]->id;
                $informacionUserGrupo->save();
            }
        }
    }

    public function enviarInformacion(){
        $grupoAcertijo = $this->ultimoGrupoAcertijo();

        $this->generarInformacion($grupoAcertijo->acertijo_id);
        $acertijo_id = $grupoAcertijo->acertijo_id;

        $user_grupos = $this->userGrupos()->get()->all();
        foreach ($user_grupos as $user_grupo) {
            $user = $user_grupo->user;

            $informacion_user_grupos = [];

            $temp = $user_grupo->informacionUserGrupos()->get()->all();
            $informacion_user_grupos = array_merge($informacion_user_grupos, $temp);

            $informacion_user_grupos = array_filter($informacion_user_grupos, function ($informacion_user_grupo) use ($acertijo_id) {
                return $informacion_user_grupo->informacion->acertijo_id === $acertijo_id;
            });

//            $count = 0;
            foreach ($informacion_user_grupos as $informacion_user_grupo){
                /*if($informacion_user_grupo){

                }
                $count++*/;
                event(new SendInformacionMailEvent($user, $informacion_user_grupo->informacion));
            }
//            if($count){
//
//            }
        }
    }
}
