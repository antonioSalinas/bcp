<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;

class GrupoAcertijo extends Model {

    //use SoftDeletes;
    protected $dates = ['deleted_at'];
    public function acertijo() {
        return $this->belongsTo('App\Acertijo', 'acertijo_id')
            ->first();
    }

    public function grupo() {
        return $this->belongsTo('App\Grupo', 'grupo_id');
    }

    public function obtenerPista($index) {
//        $offset = $this->pistas_usadas;
        return $this->hasMany('App\Pista', 'acertijo_id', 'acertijo_id')
            ->skip($index)
            ->orderBy('id', 'asc')
            ->first();
    }

    public function cantidadPistas() {
        return $this->hasMany('App\Pista', 'acertijo_id', 'acertijo_id')
            ->count();
    }

    public function cerrarAcertijoActual(Acertijo $acertijo, $pidio_respuesta = false) {
        // Calcular puntaje de tiempo final e inicial

        if ($this->estaActivado() && !$this->estaFinalizado()) {
            $this->tiempo_final = Date::now(config('app.timezone'))->format('Y-m-d H:i:s');
            if ($acertijo->tiempo > 0){
                $puntos_por_minuto = $acertijo->puntaje_inicial / $acertijo->tiempo;
            }else{
                $puntos_por_minuto = $acertijo->puntaje_inicial ;
            }
            $tiempo_en_minutos = round((strtotime($this->tiempo_final) - strtotime($this->tiempo_inicio)) / 60.0, 2);
            if ($acertijo->tiempo > 0){
                 $puntuacion_por_tiempo = round(($acertijo->tiempo - $tiempo_en_minutos) * $puntos_por_minuto, 2);
            }else{
                $puntuacion_por_tiempo = $acertijo->puntaje_inicial;

            }
            if ($pidio_respuesta)
                $puntuacion_por_tiempo -= config('game.puntos_menos_respuesta');
            if ($this->pistas_usadas > 0)
                $puntuacion_por_tiempo -= $this->pistas_usadas * config('game.puntos_menos_pista');
            $this->puntuacion = max($puntuacion_por_tiempo, 0);
        }
    }

    public function estaActivado() {
        return $this->tiempo_inicio !== null;
    }

    public function activar() {
        $this->tiempo_inicio = Date::now(config('app.timezone'))->format('Y-m-d H:i:s');
        $this->save();
    }

    public function estaFinalizado() {
        return $this->tiempo_final !== null;
    }
}
