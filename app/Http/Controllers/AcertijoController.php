<?php

namespace App\Http\Controllers;

use App\Acertijo;
use App\Grupo;
use App\GrupoAcertijo;
use App\InformacionUserGrupo;
use App\Informacione;
use App\UserGrupo;
use App\UserInfos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\TiempoAcertijo;
use App\User;

use URL;

class AcertijoController extends Controller {

    public function actual() {
        $user = Auth::user();
        $grupo = $user->enabledGroup();
        if (empty($grupo)) {
            return [
                "error" => 'Grupo no asignado.'
            ];
        }

        $grupoAcertijo = $grupo->ultimoGrupoAcertijo();
//        if(empty($grupoAcertijo) || !$grupoAcertijo->estaActivado())
//            $grupoAcertijo = $grupo->crearGrupoAcertijo();

//        if(!empty($grupoAcertijo)) {
        $acertijo = $grupoAcertijo->acertijo();
        if (isset($acertijo->media)) {

        }

        $media = null;
        if ($acertijo->media) {
            $file = json_decode($acertijo->media, false);
            if (!empty($file))
                $media = Storage::disk(config('voyager.storage.disk'))->url($file[0]->download_link);
        }

        $tiempo_en_seg = null;
        if ($grupoAcertijo->tiempo_final && $grupoAcertijo->tiempo_inicio) {
//            $grupoAcertijo->cerrarAcertijoActual($acertijo, false);
//            $grupoAcertijo->save();
            $tiempo_en_minutos = round((strtotime($grupoAcertijo->tiempo_final) - strtotime($grupoAcertijo->tiempo_inicio)) / 60.0, 2);
            $tiempo_en_seg = 60 * $tiempo_en_minutos;
        }
        $tiempos= TiempoAcertijo::where('acertijo_id', $acertijo->id)->first();
        //return json_encode($tiempos);
        isset($tiempos->p1)?$pista1t = $tiempos->p1:$pista1t =900;
        isset($tiempos->p2)?$pista2t = $tiempos->p2:$pista2t =300; 

      

        return [
            "acertijo_id" => $acertijo->id,
            "titulo" => $acertijo->titulo,
            "pregunta" => $acertijo->pregunta,
            "respuesta" => strlen($acertijo->respuesta),
            "puntaje_inicial" => $acertijo->puntaje_inicial,
            "tiempo" => $acertijo->tiempo,
            "media" => $media,
            "color" => $acertijo->color,
            "tema_css" => $acertijo->tema_css,
            "pistas_usadas" => $grupoAcertijo->pistas_usadas,
            "puntuacion" => $grupoAcertijo->puntuacion,
            "tiempo_inicio" => $grupoAcertijo->tiempo_inicio,
            "tiempo_duracion_en_segundos" => $tiempo_en_seg,
            "hora_servidor" => Date::now(config('app.timezone'))->format('Y-m-d H:i:s'),
            "activado" => $grupoAcertijo->estaActivado(),
            "finalizado" => $grupoAcertijo->estaFinalizado(),
            "feedback" => isset($acertijo->feedback) ? $acertijo->feedback : 'No existe campo feedback',
            "pista1t" => $acertijo->tiempo_p1,
            "pista2t" => $acertijo->tiempo_p2
        ];
        /*} else{
            return [
                "error" => 'No hay más acertijos.',
            ];
        }*/
    }

    public function activarActual() {
        $user = Auth::user();
        $grupo = $user->enabledGroup();
        if (empty($grupo)) {
            return [
                "error" => 'Grupo no asignado.'
            ];
        }

        $grupoAcertijo = $grupo->ultimoGrupoAcertijo();
        if (!$grupoAcertijo->estaActivado()) {
            $grupoAcertijo->activar();
//            $grupo->enviarInformacion();
            $grupo->generarInformacion($grupoAcertijo->acertijo_id);
        }

        return [
            "tiempo_inicio" => $grupoAcertijo->tiempo_inicio
        ];
    }

    public function pistaActual(int $index) {
        $user = Auth::user();
        $grupo = $user->enabledGroup();
        if (empty($grupo)) {
            return [
                "error" => 'Grupo no asignado.'
            ];
        }
        $grupoAcertijo = $grupo->ultimoGrupoAcertijo();
//        if(empty($grupoAcertijo)) {
//            return [
//                "error" => 'No hay acertijo asignado.'
//            ];
//        }

        if (!$grupoAcertijo->estaActivado()) {
            return [
                "error" => 'El acertijo no está activado.'
            ];
        }

        $pista = $grupoAcertijo->obtenerPista($index - 1);

        if (!empty($pista)) {
            if ($grupoAcertijo->pistas_usadas < $index) {
                $grupoAcertijo->pistas_usadas++;
                $grupoAcertijo->save();
            }

            return [
                "pista" => $pista->pista,
                "puntero" => $grupoAcertijo->pistas_usadas,
                "total" => $grupoAcertijo->cantidadPistas()
            ];
        } else {
            return [
                "error" => 'No existe pista.',
                "puntero" => $grupoAcertijo->pistas_usadas,
                "total" => $grupoAcertijo->cantidadPistas()
            ];
        }
    }

    public function respuestaActual() {
        $user = Auth::user();
        $grupo = $user->enabledGroup();
        $grupoAcertijo = $grupo->ultimoGrupoAcertijo();
//        if(empty($grupoAcertijo)) {
//            return [
//                "error" => 'No hay acertijo asignado'
//            ];
//        }

        if (!$grupoAcertijo->estaActivado()) {
            return [
                "error" => 'El acertijo no está activado.'
            ];
        }

        $acertijo = $grupoAcertijo->acertijo();

//        $grupoAcertijo->puntuacion -= config('game.puntos_menos_respuesta', 0);
        $grupoAcertijo->cerrarAcertijoActual($acertijo, true);
        $grupoAcertijo->save();

        return [
            "respuesta" => $acertijo->respuesta
        ];
    }

    public function responderActual(Request $request) {
        $user = Auth::user();
        $grupo = $user->enabledGroup();
        $grupoAcertijo = $grupo->ultimoGrupoAcertijo();
//        if(empty($grupoAcertijo)) {
//            return [
//                "error" => 'No hay acertijo asignado.'
//            ];
//        }

        if (!$grupoAcertijo->estaActivado()) {
            return [
                "error" => 'El acertijo no está activado.'
            ];
        }

        if ($grupoAcertijo->estaFinalizado()) {
            return [
                "error" => 'El acertijo ha finalizado.'
            ];
        }

        $acertijo = $grupoAcertijo->acertijo();
        $acerto = strtolower($acertijo->respuesta) === strtolower($request->post('respuesta'));
        $tiempo_en_seg = null;
        if ($acerto) {
            $grupoAcertijo->cerrarAcertijoActual($acertijo, false);
            $grupoAcertijo->save();
            $tiempo_en_minutos = round((strtotime($grupoAcertijo->tiempo_final) - strtotime($grupoAcertijo->tiempo_inicio)) / 60.0, 2);
            $tiempo_en_seg = 60 * $tiempo_en_minutos;
        }

        return [
            "success" => $acerto,
            "puntos" => $grupoAcertijo->puntuacion,
            "tiempo_duracion_en_segundos" => $tiempo_en_seg
        ];
    }

    public function actualSiguiente(Request $request) {
        // Se llama en el boton siguiente y enviar un correo a cada integrante
        $user = Auth::user();
        $grupo = $user->enabledGroup();
        $grupoAcertijo = $grupo->ultimoGrupoAcertijo();

        if ($grupoAcertijo->estaActivado() && $grupoAcertijo->estaFinalizado()) {
            try {
                $nuevoGrupoAcertijo = $grupo->crearGrupoAcertijo();
            } catch (\Exception $e) {
                return [
                    "success" => false,
                    "finish" => false,
                    "message" => $e->getMessage()
                ];
            }
        } else {
            return [
                "success" => false,
                "finish" => false,
                "message" => "Acertijo pendiente de finalización"
            ];
        }

        if (empty($nuevoGrupoAcertijo))
            return [
                "success" => false,
                "finish" => true,
                "message" => "No hay más acertijos"
            ];

        return [
            "success" => true,
        ];
    }

    public function grupoUsuarios(Request $request) {
        $user = Auth::user();
        $grupo = $user->enabledGroup();
        $users = $grupo->users;

        $response = ['nombre' => $grupo->nombre, 'usuarios' => []];
        foreach ($users as $user) {
            $response['usuarios'][] = [
                'nombre' => $user->name,
                'apellido' => $user->last_name,
                'email' => $user->email,
                'telefono' => $user->cellphone,
            ];
        }
        return $response;
    }

    public function rankingAcertijo(Request $request, int $index) {
        $offset = max((int)$index - 1, 0);

        $acertijo = Acertijo::orderBy('orden', 'asc')
            ->skip($offset)
            ->first();

        if (empty($acertijo))
            return [
                'error' => 'No existe acertijo'
            ];

        $grupos = DB::table('grupos', 'g')
            ->leftJoin('grupo_acertijos as ga', function ($join) use ($acertijo) {
                $join->on('g.id', '=', 'ga.grupo_id');
                $join->on('ga.acertijo_id', '=', DB::raw($acertijo->id));
            })
            ->select(
                'g.id',
                'g.nombre',
                DB::raw('ifnull(ga.puntuacion,0) as puntos'),
                DB::raw("ifnull(TIMESTAMPDIFF(SECOND, ga.tiempo_inicio,  ga.tiempo_final), 999999999) as tiempo")
            )
            ->orderBy('puntos', 'desc')
            ->orderBy('tiempo', 'asc')
            ->orderBy('g.nombre', 'asc')
            ->get()
            ->all();

        return $grupos;
    }

    public function rankingGeneral(Request $request) {
        $grupos = DB::table('grupos', 'g')
            ->leftJoin('grupo_acertijos as ga', function ($join) {
                $join->on('g.id', '=', 'ga.grupo_id');
            })
            ->select(
                'g.id',
                'g.nombre',
                DB::raw('ifnull(sum(ga.puntuacion),0) as puntos'),
                DB::raw("ifnull(sum(TIMESTAMPDIFF(SECOND, ga.tiempo_inicio,  ga.tiempo_final)), 999999999) as tiempo")
            )
            ->orderBy('puntos', 'desc')
            ->orderBy('tiempo', 'asc')
            ->orderBy('g.nombre', 'asc')
            ->groupBy('g.id')
            ->get()
            ->all();

        return $grupos;
    }

    public function userAcertijoInfo(Request $request) {
         $user = Auth::user(); 
         $grupo = $user->enabledGroup();         
         $ugs=UserGrupo::join('users', 'users.id', '=', 'user_grupos.user_id')->where('user_grupos.grupo_id',$grupo->id)->where('users.logged',1)->get();
         $grupoAcertijo = $grupo->ultimoGrupoAcertijo();
         $infos=Informacione::where('acertijo_id',$grupoAcertijo->acertijo_id)->get();
         $userInfos=UserInfos::where('user_id',$user->id)->where('acertijo_id',$grupoAcertijo->acertijo_id)->where('grupo_id',$user->grupo_id)->get();
         foreach($userInfos as $uf){
             $uf->delete();
         }
        $i=0;
        foreach($infos as $in){
            $uf = new UserInfos;
            if ((isset($ugs[$i])) ){
            $uf->user_id = $ugs[$i]->user_id;
            $uf->info_id = $in->id;
            }else{
                $i=0;
                $uf->user_id = $ugs[$i]->user_id;
                $uf->info_id = $in->id;
            }
            $uf->acertijo_id=$grupoAcertijo->acertijo_id;
            $uf->grupo_id=$grupoAcertijo->grupo_id;
            $uf->save();
            ++$i;              
        }

         $userInfos=UserInfos::where('user_id',$user->id)->where('acertijo_id',$grupoAcertijo->acertijo_id)->where('grupo_id',$user->grupo_id)->get();
         //return ($userInfos);
         
         $userGrupo = $grupo->userGrupos->where('user_id', $user->id)->first();


        $infoUserGrupos = InformacionUserGrupo::with('informacion')->where('user_grupo_id', $userGrupo->id)->get();
        $informaciones= Informacione::where('acertijo_id',$grupoAcertijo->acertijo_id)->get();
        $infos = [];

        foreach ($userInfos as $uf) {
                $informacion= Informacione::where('id',$uf->info_id)->where('acertijo_id',$grupoAcertijo->acertijo_id)->first();
                $obj= json_decode($informacion);
                $payload = json_decode($obj->informacion);
                $direccion = str_replace('user/acertijo-info','',URL::current());
                $infos[] = array('link' => ($payload[0]->download_link),"url" => $direccion.'//storage/');

       }


        return ($infos);
    }

    public function changeNameGroup(Request $request) {
        $user = Auth::user();
        $grupo = $user->enabledGroup();

        if (empty($grupo)) {
            return [
                "error" => 'Grupo no asignado.'
            ];
        }

        $grupo->nombre = $request->post('nombre');
        if(!empty($grupo->nombre))
            $grupo->save();
        else
            return ['error' => 'Nombre de grupo no puede estar en blanco.'];

        return ['success' => $grupo->save()];
    }

}
