<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\User;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if( empty(trim($request->post('username'))) || empty(trim($request->post('password'))) ) {
            return response()->json(array(
                'success' => false,
                'message' => __('app.username_and_password_are_required')
            ));
        }

        $credentials = request(['username', 'password']);
        if (!Auth::attempt($credentials) ) {
            return response()->json(array(
                'success' => false,
                'message' => __('app.wrong_username_or_password')
            ));
        }

        $user = $request->user();
        if(!$user->enabled) {
            return response()->json(array(
                'success' => false,
                'message' => __('app.wrong_username_or_password')
            ));
        }

        $group = $user->enabledGroup();

        if(empty($group)) {
            return response()->json(array(
                'success' => false,
                'message' => __('app.you_do_not_have_group_enabled')
            ));
        }

//        $api_token = $user->api_token;
//        if(empty($api_token)) {
            $api_token = Str::random(60);
            $user->api_token = hash('sha256', $api_token);
//        }

        $is_first_time = $user->is_first_time;
        $user->is_first_time = false;
        $user->logged = 1;
        $user->save();

        $response = array(
            'success' => true,
            'is_first_time' => (bool)$is_first_time
        );

        if($request->wantsJson()) {
            $response['token'] = $api_token;
        }
        $response['user'] = $user;
        return response()->json($response);
    }
    public function logout(Request $request){

        $user = User::find($request->id);
        $user->logged = 0;
        $user->save();
        return response()->json(['message'=>'Sesión cerrada', 'user'=>$user]);

    }
}
