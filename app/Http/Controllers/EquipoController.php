<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Equipo;
use App\GrupoEquipo;
use App\Grupo;



class EquipoController extends Controller
{
    public function getAllEquipos()
    {
        $equipo = Equipo::all();
        return response()->json($equipo);

    }
    function completeEquipo(){
        // $equipos = Equipo::all();
        // foreach($equipos as $equipo){
        //     $grupos = Grupo::where('equipo_id',$equipo->id)->get();
        //     foreach($grupos as $grupo){
        //         $chk = GrupoEquipo::where('grupo_id',$grupo->id)->where('equipo_id',$equipo->id)->first();
        //         if ( ! isset($chk->id)){
        //             $grupoEquipo = new GrupoEquipo;
        //             $grupoEquipo->grupo_id = $grupo->id;
        //             $grupoEquipo->equipo_id = $equipo->id;
        //             $grupoEquipo->save();
        //         }
        //     }
        // }
        return response()->json(["message" => "Exito"],200);
    } 
    function borradoMasivoEquipos(Request $request){
        $ids = json_decode($request->getContent());
        foreach($ids as $id){
            $equipo = Equipo::find($id->id);
            $equipo->delete();
            // $grupo->habilitado = 0;
            // $grupo->save();
        }
        return response()->json(["message" => "Borrado Exitoso"],200);

    }
}
