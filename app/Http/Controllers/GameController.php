<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Acertijo;

class GameController extends Controller {

    function index(Request $request) {
        return view('acertijos.index');
    }

    function welcome(Request $request) {
        if (!Auth::check()) {
            return redirect('/');
        }
        return view('acertijos.welcome');
    }

    function acertijo(Request $request) {
        if (!Auth::check()) {
            return redirect('/');
        }
        return view('acertijos.acertijo');
    }

    function ranking(Request $request) {
        if (!Auth::check()) {
            return redirect('/');
        }
        $acertijo= Acertijo::where('activo',1)->get();
        return view('acertijos.ranking',compact('acertijo'));
    }
}
