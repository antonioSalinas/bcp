<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use TCG\Voyager\Facades\Voyager;
use App\Grupo;
use App\GrupoEquipo;
use App\Informacione;
use App\GrupoAcertijo;
use App\User;
use Auth;
class GrupoController extends VoyagerBaseController
{
    public function sendInformation(Request $request, $grupoId) {
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $grupo = Grupo::find($grupoId);
        if($grupo) {
            $grupo->enviarInformacion();
        }
        return redirect(route('voyager.grupos.index'));
    }
    public function getAll(){
        return Grupo::where('habilitado',1)->get();
    }
    function resetProgress(Request $request){
        $progreso = GrupoAcertijo::where('grupo_id',$request->grupo_id)->get();
        foreach ($progreso as $progress) {
            $progress->grupo_id = $progress->grupo_id * -1;
            $progress->save();
        }
        return response()->json(["message" => "Borrado"]);
    }
    public function borradoMasivoGrupos(Request $request)
    {
        $ids = json_decode($request->getContent()); 
        foreach($ids as $id){
            $grupo = Grupo::find($id->id);
            $grupo->delete();
            // $grupo->habilitado = 0;
            // $grupo->save();
        }
        return response()->json(["message" => "Borrado Exitoso"],200);
    }
    function getGrupos(){
        return Grupo::where('habilitado',1)->get();
    }
    function cambiarNombre(Request $request)
    {

        $user = User::find($request->userId);
        $grupo = Grupo::find($user->grupo_id);
        $grupo->nombre = $request->nombre;
        $grupo->save();
        return back();
    }
    
}
