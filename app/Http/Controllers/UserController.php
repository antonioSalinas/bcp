<?php

namespace App\Http\Controllers;

use App\Imports\DataImport;
use App\Exports\ColumnasExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\RankingExport;
use Illuminate\Http\Request;
use App\User;
use App\Equipo;
use App\Grupo;
use Storage;
use App\UserGrupo;

use DB;
use Hash;
use Str;

class UserController extends Controller {

    public function import(Request $request) {
        try{
            if(request()->file('file')){
                Excel::import(new DataImport, request()->file('file'));
            }
          
            return response()->json(["mensaje"=> "Exito"]);

        }catch(\Exception $e){
            return response()->json(["mensaje"=> "Falló -> ".$e->getMessage()." - line ".$e->getLine()]);
        }
        

    }
    
    public function getUsers(){
       $users =   User::where('role_id','<>',1)->get();
       foreach($users as $user)
       {
           $chk = UserGrupo::where('user_id',$user->id)->first();
           ! isset($chk->id) ? $user->grupo_id = null:null;
           $user->save();
       }
    
       return $users;
    }
    public function getUsersFiltered(Request $request){
        if((int)$request->equipo_id == -1){
            $users =   User::where('enabled',1)->get();
        }elseif ((int)$request->equipo_id == 0){
            $grupo = Grupo::where('deleted_at',null)->get('id');
            $users =   User::where('enabled',1)->whereNotIn('grupo_id',$grupo)->get();
        }else{
            $grupo = Grupo::where('equipo_id',$request->equipo_id)->get('id');
            $idgroup = [];
            foreach($grupo as $gr){
                $idgroup[]=$gr->id;
            }
            $users =   User::where('enabled',1)->whereIn('grupo_id',$idgroup)->get();
        }
        return $users;
     }
    public function getUser($id){
        return User::find($id);
    }
    public function createUser(Request $request){
        $chk = User::where('username',$request->username)->orWhere('email',$request->email)->first();
        if(isset($chk->id)){
            return response()->json(["error" => 1,"message" => "Username o Email ya están en uso"]);
        }
        $notShow =['id','email_verified_at','api_token','remember_token','settings','created_at','updated_at','enabled','is_first_time','avatar'];
        $fields=DB::select('describe users');
        $data=array();
        foreach($fields as $field){
            
            if((  ! in_array($field->Field, $notShow) ) && !(($field->Field == 'password') && ($request->{$field->Field}=='*************************'))){
                $data =  array_merge($data,[$field->Field => $request->{$field->Field}]);
            }
            if($field->Field == 'password'){
                $data =  array_merge($data,[$field->Field => Hash::make($request->{$field->Field})]);
            }
        }
        $user =  User::create($data);
        $userGrupo= new UserGrupo();
        $userGrupo->user_id= $user->id;
        $userGrupo->grupo_id= $user->grupo_id;
        $userGrupo->save();
        return response()->json(["error" => 0 ,"message" => 'Guardado!',"user" =>$user]);
    }
    public function deleteUser(Request $request){
        $user = User::find($request->id);
        $user->enabled=0;
        $user->save();
        return response()->json(["message" => 'Borrado!',"user" =>$user]);
    }
    public function saveUser(Request $request){
        $user = User::find($request->id);
        $grupo_id=0;
        $notShow =['id','email_verified_at','api_token','remember_token','settings','created_at','updated_at','enabled','is_first_time','avatar'];
        $fields=DB::select('describe users');
        $data=array();
        foreach($fields as $field){
            if((  ! in_array($field->Field, $notShow) ) && !(($field->Field == 'password') && ($request->{$field->Field.'_'.$request->id}=='*************************'))){
                $data =  array_merge($data,[$field->Field => $request->{$field->Field.'_'.$request->id}]);

            }
            if($field->Field == 'password'){
                if ($request->{$field->Field.'_'.$request->id}<>'*************************'){
                    $data =  array_merge($data,[$field->Field => bcrypt($request->{$field->Field.'_'.$request->id})]);
                }
            }
            $field->Field == 'grupo_id'? $grupo_id=$request->{$field->Field.'_'.$request->id}:null;
        }

        $deleteGroup = UserGrupo::where('user_id',$user->id)->get();
        foreach ($deleteGroup as $del){
            $del->delete();
        } 
        $userGrupo= new UserGrupo();
        $userGrupo->user_id= $user->id;
        $userGrupo->grupo_id= $grupo_id;
        $userGrupo->save();

       $user->update($data);
       $user->fill($data)->save();
       return response()->json(["message"=>"Guardado","user"=>$user],200);
    }
    public function createUserField(Request $request)
    {
        $columna=$request->columna;
        $columna = strtolower(str_replace(' ','_',$columna));
        $newField = DB::select("ALTER TABLE users ADD COLUMN ".$columna." varchar(200)  DEFAULT ''");
        return response()->json(["message"=>"Creado"],200);
    }
    public function getAllFields()
    {
        $fields = DB::select('describe users');
        return $fields;
    }

   function deleteColumn(Request $request){
    $fields = DB::select("ALTER TABLE users DROP COLUMN $request->column");
    return response()->json(["mensaje"=>"Columna Eliminada"],200);
   }
   public function getAllFieldsExcel()
    {
        return Storage::download('public/plantilla_samay.xlsx');
       // return Excel::download(new ColumnasExport, 'usuarios.xlsx');
    }
    public function softDeleteAll(Request $request){
        // $recibe = Str::of($request->ids)->replace('[','');
        // $recibe = Str::of($recibe)->replace(']','')->explode(',');
        $users = User::whereRaw(' enabled = 1 ')->whereRaw('role_id <> 1')->whereRaw(" id in ($request->ids) ")->get();
        //return $users;
        foreach($users as $user){
            $user->enabled = 0;
            $user->save();
        }
        return response()->json(["message" => "Se eliminó usuarios"]);
    }
    public function getGruposFiltered(Request $request){
        
        return response()->json(Grupo::where('equipo_id',$request->equipo_id)->get());
    }
    public function getExcelRanking(Request $request){
        return Excel::download(new RankingExport($request->filter), 'report.xlsx');
    }

}
