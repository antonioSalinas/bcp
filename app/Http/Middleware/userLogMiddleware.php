<?php

namespace App\Http\Middleware;

use Closure;
use App\UserLog;
use App\User;

class userLogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $log = new UserLog;
        $log->input = strip_tags($request->getContent());
        $log->save();
        $request->log_id = $log->id;
        return $next($request);
    }
    public function terminate($request, $response)
    {
       $log = UserLog::find($request->log_id);
       $log->output = strip_tags($response);
       $log->user_info = auth()->user();
       $log->save();
    }
}
