<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class DataImport implements WithMultipleSheets 
{
    /**
    * @param Collection $collection
    */
    public function sheets(): array
    {
      
            return [
                'GRUPOS' => new EquipoImport(),
                'EQUIPOS' => new GrupoImport(),
                'USUARIOS' => new UsersImport()
        ];
    }
}
