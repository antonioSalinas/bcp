<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use App\Equipo;

class EquipoImport implements OnEachRow
{
    
    public function onRow(Row $row) {
        $rowIndex = $row->getIndex();
        $row      = $row->toArray();
        if(($row[0] == null) ||($row[0] === '') || ($row[0] === 'DESCRIPCION')){
            return null;
        }
        $equipo = Equipo::where('descripcion',$row[0])->first();
        if(! isset($equipo->id) ){
            $equipo = new Equipo;
        }
        $equipo->descripcion = $row[0];
        $equipo->save();
        return $equipo;
    }
}
