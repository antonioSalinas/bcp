<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use App\Equipo;
use App\Grupo;
use App\GrupoEquipo;


class GrupoImport implements OnEachRow
{

    public function onRow(Row $row) 
    {
        try{
            $rowIndex = $row->getIndex();
            $row      = $row->toArray();
            if(($row[0] == null) ||($row[0] === '') || ($row[0] === 'GRUPO')){
                return null;
            }
            $equipo = Equipo::where('descripcion',trim($row[0]))->first();
            isset($equipo->id) ? $idEquipo = $equipo->id : $idEquipo = 0;
            $grupo = Grupo::where('nombre',$row[1])->first();
            if( ! isset($grupo->id)){
                $grupo = new Grupo;
            }
            $grupo->nombre        = $row[1];
            $grupo->habilitado    = 1;
            $grupo->equipo_id     = $idEquipo;
            $grupo->save();
            $gequipo = new GrupoEquipo;
            $gequipo->equipo_id = $grupo->id;
            $gequipo->grupo_id = $equipo->id;
            $gequipo->save();
            return $grupo;
        }catch(\Exception $e){
            return null;
        }
    }
}
