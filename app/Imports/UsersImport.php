<?php

namespace App\Imports;

use App\User;
use App\Grupo;
use App\Equipo;
use App\UserGrupo;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Log;
use DB;
class UsersImport implements OnEachRow {
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function onRow(Row $row) {
        try{
           
        $rowIndex = $row->getIndex();
        $row      = $row->toArray();
        if(($row[0] == null) ||($row[0] === '') || ($row[0] === 'NOMBRE')){
            return null;
        }

        $user = User::where('email', $row[3])
            ->orWhere('username', $row[2])
            ->first();

        if (empty($user))
            $user = new User([
                'username' => $row[2],
                'email' => $row[3],
                'password' => \Hash::make($row[5]),
            ]);
        else {
            $user->username = $row[2];
            $user->password = \Hash::make($row[5]);
        }
        if($this->comprueba("last_name")){
            $user->last_name = $row[1];
        }
        if($this->comprueba("cellphone")){
            $user->cellphone = $row[4];
        }
        if($this->comprueba("nombre")){
            $user->nombre = $row[0];
        }
        if($this->comprueba("apellido")){
            $user->apellido = $row[1];
        }
        $user->enabled = true;
        $user->is_first_time = true;
        //6 -> equipo // 7->grupo
        //die($row[6]);
        $grupo = Grupo::where('nombre',$row[5])->first();
        isset($grupo->id) ? $grupoId = $grupo->id : $grupoId = 0; 
        $equipo = Equipo::where('grupo_id',$grupoId)->first();
        isset($equipo->id) ? $equipoId = $equipo->id : $equipoId = 0; 
        $user->grupo_id = $grupoId;
        $user->save();
        $userGrupo = UserGrupo::where('user_id',$user->id)->first(); 
        isset($userGrupo->id)?$userGrupo->delete():null;
        $userGrupo= new UserGrupo();
        $userGrupo->user_id= $user->id;
        $userGrupo->grupo_id= $grupoId;
        $userGrupo->save();
        return $user;
    }catch(\Exception $e){
        Log::info($e);
        return null;

    }
        
}
private function comprueba($columna){
    $todas = DB::select("describe users") ;
    foreach($todas as $td)
    {
        if($td->Field == $columna){
            return true;
        }
    }
    return false;
}
}
