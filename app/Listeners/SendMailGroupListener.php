<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\InformationAcertijo;
use Illuminate\Support\Facades\Mail;

class SendMailGroupListener implements ShouldQueue {
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     * @return void
     */
    public function handle($event) {
        $user = $event->user;
        $informacion = $event->informacion;
        if($user) {
            Mail::to($user)->send(new InformationAcertijo($user, $informacion));
        }
    }
}
