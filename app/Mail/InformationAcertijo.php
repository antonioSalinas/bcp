<?php

namespace App\Mail;

use App\User;
use App\Informacione;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InformationAcertijo extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $informacion;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Informacione $informacion)
    {
        $this->user = $user;
        $this->informacion = $informacion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.acertijo.information');
    }
}
