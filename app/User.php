<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Yadahan\AuthenticationLog\AuthenticationLogable;
use Yadahan\AuthenticationLog\AuthenticationLog;
use App\CustomUserData;


class User extends \TCG\Voyager\Models\User {
    use Notifiable, AuthenticationLogable;


    public $additional_attributes = ['full_name', 'locale'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password', 'enabled',
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot() {
        parent::boot();
        User::deleting(function ($model) {

            $user_grupos = UserGrupo::where('user_id', $model->id)->get()->all();
            foreach ($user_grupos as $user_grupo) {
                InformacionUserGrupo::where('user_grupo_id', $user_grupo->id)->delete();
                $user_grupo->delete();
            }
        });
    }

    public function scopeEnabled($query) {
        return $query->where('enabled', 1);
    }

    public function grupos() {
        return $this->belongsToMany('App\Grupo', 'user_grupos');
    }

    public function enabledGroup() {
        return $this->belongsToMany('App\Grupo', 'user_grupos')
            ->where('habilitado', 1)
            ->first();
    }

    public function getFullNameAttribute() {
        return "{$this->name} {$this->last_name}";
    }

    public function getEnabledBrowseAttribute() {
        return $this->enabled ? __('voyager::generic.yes') : __('voyager::generic.no');
    }

    public function getEnabledReadAttribute() {
        return $this->enabled ? __('voyager::generic.yes') : __('voyager::generic.no');
    }

    public function scopePlayers($query) {
        return $query->where('enabled', 1)
            ->whereDoesntHave('role', function ($query) {
                $query->where('name', 'admin');
            });
    }

    public function scopeFreePlayers($query) {
        return $query->players()
            ->whereDoesntHave('grupos', function ($query) {
                $query->where('habilitado', 1);
            });
    }

    public function authenticationsReverse()
    {
        return $this->morphMany(AuthenticationLog::class, 'authenticatable')->oldest('login_at');
    }

    public function firstLoginAt()
    {
        return optional($this->authenticationsReverse()->first())->login_at;
    }

}
