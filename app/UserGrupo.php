<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UserGrupo extends Model {

    public function informaciones() {
        return $this->belongsToMany('App\Informacione', 'informacion_user_grupos', 'user_grupo_id', 'informacion_id');
    }

    public function informacionUserGrupos() {
        return $this->hasMany('App\InformacionUserGrupo', 'user_grupo_id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
