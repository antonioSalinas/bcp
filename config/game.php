<?php
return [
    'puntos_menos_pista' => env('PUNTOS_MENOS_PISTA', 20),
    'puntos_menos_respuesta' => env('PUNTOS_MENOS_RESPUESTA', 1000),
];
