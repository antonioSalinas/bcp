<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('username')->after('role_id')->nullable()->unique();
            $table->string('last_name', 100)->nullable()->after('name');
            $table->string('cellphone', 15)->nullable()->after('email');
            $table->boolean('enabled')->default(true)->nullable();
            $table->boolean('is_first_time')->default(true)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['username', 'last_name', 'cellphone', 'enabled', 'is_first_time']);
        });
    }
}
