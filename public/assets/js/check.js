
var formAcertijo = document.getElementById('form-acertijo')
var divAnswer = document.getElementById('div-answer')

var numAcertijo
var puntos

var urlAcertijo = 'acertijo/actual'
var urlResponder = 'acertijo/actual/responder'



formAcertijo.addEventListener('submit', function(e){
    e.preventDefault()

    var formData = new FormData(formAcertijo);

    if (!(formData.get('respuesta')==="")){

        validar(formData)

    }else{
        error()
    }
})

function validar(formData){         // Validamos si la respuesta es correcta

    fetch(urlResponder,{
        method:'POST',
        body:formData
    })
    .then(res => res.json())
    .then(data =>{

        if(data.success){
            correct()
        }else{
            error();
        }
    })
    .catch(function(e){
        
    })
}

function correct(){         // Mostramos el feedback y puntaje

    divAnswer.innerHTML=`
        <div id="bg-answer-ok">
            RESPUESTA CORRECTA
        </div>
    `

    setTimeout(function(){

         cargarFeedback()

    },1500)
}

function error(){                   // Error en respuesta

    divAnswer.innerHTML=`
        <div class="bg-answer-e">
            RESPUESTA INCORRECTA
        </div>
    `

    setTimeout(function(){

        fetch(urlAcertijo)
        .then(res => res.json())
        .then(data =>{

            divAnswer.innerHTML=`
                <div class="bg-answer">
                    <div id="bg-clave"></div>   
                    <input type="text" placeholder="CLAVE" name="respuesta" id="clave" class="input text-color border-color" onKeyUp="checkLen()">
                    <button id="btn-comprobar" type="submit">COMPROBAR</button>
                </div>
            `

            for(let i =  0 ; i < data.respuesta; i++){
                document.getElementById('bg-clave').innerHTML +=`
                    <span class="border-color"></span>
                `
            }

        })

        divAnswer.innerHTML=`
        <div class="bg-answer">
            <div id="bg-clave"></div>   
            <input type="text" placeholder="CLAVE" name="respuesta" id="clave" class="input text-color border-color" onKeyUp="checkLen()">
            <button id="btn-comprobar" type="submit">COMPROBAR</button>
        </div>
    `

    },1500)

}
