window.onload = () => {
    addGrupos()
    document.getElementById('bulk_delete_btn').onclick = () => borradoMasivo() 
}

addGrupos = () => {
    init = {
        method : "POST"
    }
    url = '/api/completeEquipo'
    fetch(url,init)
    .then(res => res.json())
    .then(data =>{
        console.log(data.message)
    })
    //location.reload()
    
  
}

borradoMasivo = () =>{
    var tosend = []
    cheques = document.querySelectorAll("input[type=checkbox]")
    cheques.forEach(check =>{
        if ((check.checked)&&( ! check.classList.contains('select_all'))){
            tosend.push(
                {
                    id: check.value
                }
            )
        } 
    })
    console.log(tosend)
    url = '/api/borradoMasivoEquipos'
    data = tosend
    init = {
        method: 'POST', 
        body: JSON.stringify(data), 
        headers:{
          'Content-Type': 'application/json'
        }
    }
    fetch(url,init)
    .then(res => res.json())
    .then(data =>{
        alert(data.message)
    })
    location.reload()
}
