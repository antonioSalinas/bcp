// var btnFeedback = document.getElementById('btn-feedback')

var urlActual = 'acertijo/actual'
var urlRespuesta = 'acertijo/actual/respuesta'
var urlSiguiente = 'acertijo/actual/siguiente'
var acertijoActual = 0
var dataActual
var infoGrupo

// var vid = document.getElementById('videoFin')

function cargarFeedback(){

    // document.getElementById('acertijo-info').innerHTML=`
    //     <div class="bg-feedback">
    //         <span id="total-time">00:00</span>
    //         <span id="total-points">Hiciste un total de 0 puntos</span>
    //     </div>
    // `
    setInterval(() => {
        fetch(urlActual)
        .then(res => res.json())
        .then(data =>{
            //console.log(data)
            if((acertijoActual!=data.acertijo_id)&&(acertijoActual!=0)){
                location.reload()
            }else{
                acertijoActual=data.acertijo_id
            }

        })

    },3000)

    document.getElementById('acertijo-form').classList.add('hide')
    document.getElementById('acertijo-form').classList.remove('show')

    document.getElementById('feedback-content').classList.remove('hide')
    document.getElementById('feedback-content').classList.add('show')


    fetch(urlRespuesta)
    .then(res => res.json())
    .then(data =>{

        document.getElementById('info-solucion').innerHTML= `LA RESPUESTA ES : ${data.respuesta}`

    })



    fetch(urlActual)
    .then(res => res.json())
    .then(data =>{

        var tiempo = parseInt(data.tiempo_duracion_en_segundos)
        var min = parseInt(tiempo/60)
        var sec = tiempo - (min * 60)

        if(min<10){ min = `0${min}` }
        if(sec<10){sec = `0${sec}`}

        document.getElementById('acertijo-info').innerHTML=`
            <div class="bg-feedback">
                <span id="total-time">${min}:${sec}</span>
                <div id="feedback-info" class="text-color-default">${data.feedback} </div>
                <span id="total-points">HICIERON UN TOTAL DE ${data.puntuacion} PUNTOS</span>
            </div>
            `


        // document.getElementById('total-time').innerText = `${min}:${sec}`
        // document.getElementById('total-points').innerText = `HICIERON UN TOTAL DE ${data.puntuacion} PUNTOS`
        // document.getElementById('info-feedback').innerHTML= `${data.feedback}`



        setTimeout(() => {

            document.getElementById("btnFeedback").innerHTML = '<button id="btn-feedback" type="button">SIGUIENTE DESAFÍO</button>'


            // if(data.tema_css == "red")
            // {
            //     document.getElementById('btn-feedback').innerText = "Ver historia de Adriana"
            //     // setTimeout(() => {
            //     //     verVideo()
            //     // },3000);
            // }

        }, 20000);

    })

}

btnFeedback.onclick = function(){

    fetch(urlSiguiente)
    .then(res => res.json())
    .then(data => {
        if(data.finish){
            document.getElementById("btnFeedback").innerHTML = '<button id="btn-feedback" type="button">NO HAY MAS DESAFIOS</button>'
            alert('no hay mas desafios')
            // window.location = "http://youtube.com"
            // var win = window.open('http://stackoverflow.com/', '_blank');
           // verVideo()
            return
        }

        window.location = "acertijo"

    })
    .catch(error => {
    })
}

function verVideo(){
    document.getElementById('video-panel').classList.remove('hide')
    document.getElementById('video-panel').classList.add('show')
    // vid.play()
}


document.getElementById('closeVideo').onclick = function(){

    document.getElementById('videoFin').innerHTML = ''
    document.getElementById('video-panel').classList.remove('show')
    document.getElementById('video-panel').classList.add('hide')

    // vid.pause()

    // window.location = "acertijo"

}

