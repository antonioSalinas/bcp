var allRows
window.onload = () =>{
    fillCombo()
    hideDeleted()
    allRows = document.querySelector('tbody').innerHTML
    var button = document.createElement('button')
    button.classList.add('btn','btn-primary', 'btn-add-new')
    button.innerHTML ='Exportar Ranking'
    button.onclick = () => exportRanking()
    document.getElementById('botones').appendChild(button)
    var btnDelAcert = document.createElement('button')
    btnDelAcert.classList.add('btn','btn-danger', 'btn-add-new')
    btnDelAcert.innerHTML ='Reset Progreso Grupo'
    btnDelAcert.setAttribute("data-toggle","modal")
    btnDelAcert.setAttribute( "data-target","#exampleModal")
    document.getElementById('botones').appendChild(btnDelAcert)        
    init = {
        method : 'POST'
    }     
    url = '/api/getGrupos'
    fetch(url,init)
    .then(res => res.json())
    .then(data =>{
        var label = document.createElement('label')
        label.innerHTML= 'Seleccione un Grupo'
        document.getElementById('modal-form').appendChild(label)
        var combo = document.createElement('select')
        combo.id = 'combo_grupos'
        combo.classList.add('form')
        combo.classList.add('form-control','form-control-sm')
        var dmyopt = document.createElement('option')
        dmyopt.value = -1
        dmyopt.innerHTML = 'TODOS'
        combo.appendChild(dmyopt)
        dmyopt = document.createElement('option')
        dmyopt.value = 0
        dmyopt.innerHTML = 'SIN EQUIPO'
        combo.appendChild(dmyopt)
        data.forEach(element => {
            var opt = document.createElement('option')
            opt.value = element.id
            opt.innerHTML = element.nombre
            combo.appendChild(opt)
        });
        document.getElementById('modal-form').appendChild(combo)
    })
    spans = document.querySelectorAll('span')
    spans.forEach(span =>{
        if (span.innerHTML == "Borrado masivo")
        {
            span.onclick = () => borradoMasivo()
        }
    })
    addGrupos()
    links = document.querySelectorAll('a')
    links.forEach(link => {
        if(link.title == 'Enviar información'){
            link.style.visibility = "hidden"
        }
    })
    
}


addGrupos = () => {
    init = {
        method : "POST"
    }
    url = '/api/completeEquipo'
    fetch(url,init)
    .then(res => res.json())
    .then(data =>{
        console.log(data.message)
    })
    //location.reload()
}
borradoMasivo = () =>{
    var tosend = []
    cheques = document.querySelectorAll("input[type=checkbox]")
    cheques.forEach(check =>{
        if ((check.checked)&&( ! check.classList.contains('select_all'))){
            tosend.push(
                {
                    id: check.value
                }
            )
        }
    })
    url = '/api/borradoMasivoGrupos'
    data = tosend
    init = {
        method: 'POST', 
        body: JSON.stringify(data), 
        headers:{
          'Content-Type': 'application/json'
        }
    }
    fetch(url,init)
    .then(res => res.json())
    .then(data =>{
        alert(data.message)
    })
    location.reload()
}
fillCombo = () => {
    var myRequest = new Request('/api/getAllEquipos')                
    fetch(myRequest)
    .then(res => res.json())
    .then(data =>{
        var combo = document.createElement('select')
        combo.id = 'sel_equipo'
        combo.onchange = () => filterTable(combo.value)
        combo.classList.add('form')
        combo.classList.add('form-control')
        var dmyopt = document.createElement('option')
        dmyopt.value = -1
        dmyopt.innerHTML = 'TODOS'
        combo.appendChild(dmyopt)
        dmyopt = document.createElement('option')
        data.forEach(element => {
            var opt = document.createElement('option')
            opt.value = element.id
            opt.innerHTML = element.descripcion
            combo.appendChild(opt)
        });
        document.getElementById('combo_equipos').appendChild(combo)
        

    }).catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response));
}
filterTable = () =>{
    document.querySelector('tbody').innerHTML = allRows
    var filter = document.getElementById('sel_equipo')
    var text= filter.options[filter.selectedIndex].innerHTML
    if(! text.includes('TODOS')){
        var tbody = document.querySelector('tbody');
        var terres = tbody.querySelectorAll('tr')
        terres.forEach(tr => {
            var tedes = tr.querySelectorAll('td')
            var includes = 0
            tedes.forEach(td => {
                if(td.innerHTML.includes(`<p>${text}</p>`))
                {
                includes = 1
                } 
                
            })
            if(includes == 0){
                tr.remove()
            }
        });
    }
}

exportRanking = () =>{
    var checks = document.querySelectorAll('input[type=checkbox]')
    var filtro = []
    checks.forEach(chk => {
       // alert(chk.value)
        if(! isNaN(parseInt(chk.value)))
        {
            filtro.push({id : parseInt(chk.value)})
        }
    });
    var datos={
        filter : filtro
    }
    url = '/api/getExcelRanking'
    init = {
        method: 'POST', 
        body: JSON.stringify(datos), 
        headers:{
          'Content-Type': 'application/json'
        }
    }  
    fetch(url,init)
    .then(res => res.blob())
    .then(data =>{
        var a = document.createElement("a")
        document.body.appendChild(a)
        a.style = "display: none"
        url = window.URL.createObjectURL(data)
        a.href = url
        a.download = 'Ranking.xlsx'
        a.click()
    })

}
resetProgress = () => {
    if(confirm('Va a resetear el progreso del grupo'))
    {
        formdata = new FormData;
        formdata.append('grupo',document.getElementById('combo_grupos').value)
        url = '/api/resetProgress'
        init = {
            method: 'POST', 
            body : formdata
        }  
        
        fetch(url,init)
        .then(res => res.json())
        .then(data =>{
        alert(data.message)
        })
    }
    
} 
hideDeleted = () =>{
    tedes = document.querySelectorAll('td')
    tedes.forEach(td =>{
        if(td.innerHTML.includes('No')){
            td.parentElement.setAttribute("hidden","hidden")
        }
    })
}