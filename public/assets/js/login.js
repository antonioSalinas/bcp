
var formulario = document.getElementById('form-index')
var respuesta = document.getElementById('btn-login')

formulario.addEventListener('submit', function(e){
    e.preventDefault()

    var formData = new FormData(formulario);

    if (!(formData.get('username')==="") && !(formData.get('password')==="")){

        validarLogin(formData)

    }else{
        error()
    }
})

function validarLogin(formData){
    let duracion = 1500;

    fetch('login',{
        method:'POST',
        body:formData                    
    })
    .then(res => res.json())
    .then( data =>{

        sessionStorage.setItem('user',data.user.id);
        respuesta.innerHTML = `
            <div id="bg-index-button">
                <button id="index-button">Bienvenido !!! </button>
            </div>
        `

        if(data.success){
            if(data.is_first_time){

                setTimeout(function(){
                    window.location="welcome";
                },duracion);

            }else{
                
                setTimeout(function(){
                    
                    window.location="acertijo";
                },duracion);
            }

        }else{
            error();
        }
    })
    .catch(function(e){
        error()
    })

}

function error(){
    let duracion = 2500;

    respuesta.innerHTML = `
        <div id="bg-index-button-e">
            <button id="index-button" type="submit">Datos incorrectos</button>
        </div>
    `
    setTimeout(function(){
        respuesta.innerHTML = `
            <div id="bg-index-button">
                <button id="index-button" type="submit"> Ingresar</button>
            </div>
        `
    },duracion)
}




