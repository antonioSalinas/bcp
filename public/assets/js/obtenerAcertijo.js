
var numAcertijo = document.getElementById('num_acertijo')
var infoAcertijo = document.getElementById('info_acertijo')

var formAcertijo = document.getElementById('form-acertijo')

var urlAcertijo = 'acertijo/actual'
var urlActivar = 'acertijo/actual/activar'
var urlDescarga = 'user/acertijo-info'
var maxlen = 0



inicio()



function inicio(){              // COMPROBAMOS SI EL ACERTIJO SE INICIO O NO

    fetch(urlAcertijo)
    .then(res => res.json())
    .then( data =>{

        switch (data.tema_css) {
            case 'orange':
                //document.getElementById('acertijo-title').innerHTML=`Desafío N°1`
                document.getElementById('theme').href = 'assets/css/themeOrange.css'
            break;

            case 'cyan':
                //document.getElementById('acertijo-title').innerHTML=`Desafío N°2`
                document.getElementById('theme').href = 'assets/css/themeCyan.css'
            break;

            case 'yellow':
                //document.getElementById('acertijo-title').innerHTML=`Desafío N°3`
                document.getElementById('theme').href = 'assets/css/themeYellow.css'
            break;

            case 'green':
                //document.getElementById('acertijo-title').innerHTML=`Desafío N°4`
                document.getElementById('theme').href = 'assets/css/themeGreen.css'
            break;

            case 'magenta':
                //document.getElementById('acertijo-title').innerHTML=`Desafío N°5`
                document.getElementById('theme').href = 'assets/css/themeMagenta.css'
            break;

            case 'red':
                //document.getElementById('acertijo-title').innerHTML=`Desafío N°6`
                document.getElementById('theme').href = 'assets/css/themeRed.css'
            break;

            default:
            break;
        }


        // VERIFICACION DE INICIALIZACIÓN
        if(data.activado){
            if(data.finalizado){
                cargarFeedback()
            }
            else{
                obtenerAcertijo()
            }
        } else{
            inicializarAcertijo()
        }

    })
    .catch(error => {
    })

}

function inicializarAcertijo(){ // INICIALIZAMOS ACERTIJO CON EL BTN GO

    formAcertijo.classList.add('hide')
    formAcertijo.classList.remove('show')
    // document.getElementById('acertijo-info').innerHTML=`
    // <div id="bg-iniciar">
    //     <div class="bg-btn-iniciar">
    //         <button type="button" id="btn-go" onclick=iniciarAcertijo()>GO</button>
    //     </div>
    //     <h2>
    //         Al hacer click en <b>GO</b>, iniciará el juego y comenzará a correr el tiempo para todo tu equipo.<br><br>
    //         ¿Ya coordinaste con tu equipo?
    //     </h2>
    // </div>
    // `
    document.getElementById('acertijo-info').innerHTML=`
    <div id="bg-iniciar">
        <div class="bg-btn-iniciar">
            <button type="button" id="btn-go">GO</button>
        </div>
        <h2>
            Al hacer click en <b>GO</b>, iniciará el juego y comenzará a correr el tiempo para todo tu equipo.<br><br>
            ¿Ya coordinaste con tu equipo?
        </h2>
    </div>
    `
    console.log('go creado')
    var btngo = document.getElementById('btn-go')
    btngo.addEventListener('click', function(e){
        e.preventDefault()
        iniciarAcertijo()
        console.log('go pulsado')
    })
}

function iniciarAcertijo(){     // GRABAMOS EN BD EL INICIO DEL ACERTIJO

    formAcertijo.classList.add('show')
    formAcertijo.classList.remove('hide')

    fetch(urlActivar)
    .then(res => res.json())
    .then(data => {
        
        obtenerAcertijo()
    })
    .catch(error => { })
 

}

function obtenerAcertijo(){     // OBTENEMOS INFORMACIÓN COMPLETA DEL ACERTIJO
    
    fetch(urlAcertijo)
    .then(res => res.json())
    .then(data =>{
        maxlen= data.respuesta
        for(let i =  0 ; i < data.respuesta; i++){
            document.getElementById('bg-clave').innerHTML +=`
                <span class="border-color"></span>
            `
        }
        
        document.getElementById('acertijo-title').innerHTML = data.titulo
        document.getElementById('img-title').classList.remove('img-titleA')
        document.getElementById('img-title').classList.add('img-titleB')
        document.getElementById('acertijo-header').classList.remove('text-color')
        document.getElementById('acertijo-header').classList.add('text-color-default')
        

        if(data.media){

            document.getElementById('acertijo-info').innerHTML=`
            <div class="bg-audio">
                <label id="timer">0</label>
                <div class="bg-btn-audio">
                    <button type="button" id="play-audio" onclick=playMedia()></button>
                    <button type="button" id="btn-info" onclick=descargar()>Descargar Información</button>
                </div>
            </div>
            `
        }
        else{
            
            document.getElementById('acertijo-info').innerHTML=`
            <div class="bg-info">
                <label id="timer">0</label>
                <div id="text-info" class="text-color-default">${data.pregunta} </div>
                <button type="button" id="btn-info" onclick=descargar()>Descargar Información</button>
            </div>
            `
        }

    })
    .catch(error =>{
    })

    iniciarTimer()
}

function descargar(){

    fetch(urlDescarga)
    .then(res => res.json())
    .then(data=>{
        //console.log(data);
        var tempFile = document.createElement("a");
        tempFile.style.display = 'none';

        document.body.appendChild(tempFile)

        for (let i = 0 ; i < data.length; i++){
                var dataParsed = JSON.parse(JSON.stringify(data))
                console.log(dataParsed);
                 tempFile.setAttribute('href','controlpanel/'+dataParsed[i].link)
                 tempFile.setAttribute('download',`documento_${i}`)

                 tempFile.click()
        }
        document.body.removeChild(tempFile)

    })

}

function checkLen(){
    var x = document.getElementById("clave")
    if(x.value.length  > maxlen)
    {
        x.value = x.value.substring(0,maxlen)
    } 
}





