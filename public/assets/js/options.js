var team = document.getElementById('user-panel')
var teamList = document.getElementById('team-list')
var rules = document.getElementById('rules-panel')
var formTeam = document.getElementById('formTeam')
var urlNombreGrupo = 'user/cambiar-nombre-grupo'


document.getElementById('btn-logout').onclick = function(){
    var myRequest = new Request(`api/logout?id=${sessionStorage.getItem('user')}`)
    fetch(myRequest)
    .then(res => res.json())
    .then(data => {
        console.log(data);
    })
    .catch(error => {
        console.log(error)
    })
    window.location="/";

}


document.getElementById('btn-ranking').onclick = function(){
    window.location="ranking";
}

document.getElementById('btn-team').onclick = function(){

    getTeam()
}

document.getElementById('closeTeam').onclick = function(){
    team.classList.remove('show')
    team.classList.add('hide')
}

/* document.getElementById('btn-rules').onclick = function(){
    rules.classList.remove('hide')
    rules.classList.add('show')
}
*/

document.getElementById('closeRules').onclick = function(){
    rules.classList.remove('show')
    rules.classList.add('hide')
}

function getTeam(){

    team.classList.remove('hide')
    team.classList.add('show')

    fetch('grupo/usuarios')
    .then(res => res.json())
    .then(data => {
        getDataTeam (data)
    })


}

function getDataTeam(data){

    document.getElementById('groupName').value = data.nombre

    teamList.innerHTML=''
    for(let valor of data.usuarios){
        teamList.innerHTML +=`
        <li>
            <div>${valor.nombre} ${valor.apellido}</div>
            <div>${valor.email}</div>
        </li>

        `
    }

}


formTeam.addEventListener('submit', function(e){
    e.preventDefault()

    var formData = new FormData(formTeam)

    if(!(formData.get('nombre')==="")){

        validaNombre(formData)

    }else{

        errorGrupo()

    }

})

function validaNombre(formData){

    fetch(urlNombreGrupo,{
        method:'POST',
        body:formData
    })
    .then(res => res.json())
    .then(data => {

        if(data.success){
            nombreGrupo()
        }else{
            errorGrupo()
        }
    })
    .catch(error => { })
}

function nombreGrupo(){
    fetch('grupo/usuarios')
        .then(res => res.json())
        .then(data => {
            document.getElementById('groupName').value = data.nombre
    })
}

function errorGrupo(){
    document.getElementById('btnGroupName').innerText = "ERROR"

    setTimeout(function(){
        document.getElementById('btnGroupName').innerText = "GUARDAR"
    },1500)
}

