
var btnPista1 = document.getElementById('tip1')
var btnPista2 = document.getElementById('tip2')
var btnSolucion = document.getElementById('solution')

var pistaPanel = document.getElementById('tip-panel')
var pistaContent = document.getElementById('tip-content')

var urlAcertijo = 'acertijo/actual'
var urlPista

var verPista1
var verPista2
var idPista
var tituloPista
var infoPista
var numAcertijo


function iniciarPista1()
{
    btnPista1.disabled = false
}

function iniciarPista2()
{
    btnPista2.disabled = false
}

btnPista1.onclick = function(){                     // ACCIONES DE PISTA 1

    pistaPanel.classList.remove('hide')
    pistaPanel.classList.add('show')

    fetch('acertijo/actual/pista/1')
    .then(res => res.json())
    .then(data =>{
    
        idPista ="tip1"

        pistaContent.innerHTML=`
            <div class="bg-pista">
                <span class="tip-close" id="tip-close" onclick=cerrarPista(idPista)></span>
                <div class="tip-warning">
                    <h1>Pista 1</h1>
                    <p>
                        ${data.pista}
                    </p>
                </div>
            </div>  
        
            <div class="info-tip-show show" id="show-tip1">PISTA 1</div>
            <div class="info-tip-show hide" id="show-tip2">PISTA 2</div>
            <div class="info-tip-show hide" id="show-solution">SOLUCIÓN</div>
        `


    })
}

btnPista2.onclick = function(){                     // ACCIONES DE PISTA 1

    pistaPanel.classList.remove('hide')
    pistaPanel.classList.add('show')

    fetch('acertijo/actual/pista/2')
    .then(res => res.json())
    .then(data =>{
    
        idPista ="tip2"

        pistaContent.innerHTML=`
            <div class="bg-pista">
                <span class="tip-close" id="tip-close" onclick=cerrarPista(idPista)></span>
                <div class="tip-warning">
                    <h1>Pista 2</h1>
                    <p>
                        ${data.pista}
                    </p>
                </div>
            </div>  
        
            <div class="info-tip-show hide" id="show-tip1">PISTA 1</div>
            <div class="info-tip-show show" id="show-tip2">PISTA 2</div>
            <div class="info-tip-show hide" id="show-solution">SOLUCIÓN</div>
        `
    })
}


btnSolucion.addEventListener('click',function(e){
    e.preventDefault()
    pistaPanel.classList.remove('hide')
    pistaPanel.classList.add('show')

    urlPista = 'acertijo/actual/respuesta'
    idPista = 'solucion'

    pistaContent.innerHTML=`
        <div class="bg-pista">
            <div class="tip-warning">
                <h1>SOLUCIÓN</h1>
                <p>
                    <br>
                    Ver la solución, <b>te quitará todos los puntos en este acertijo</b> <br>
                    ¿Estas seguro que quieres ver la solución?
                </p>
            </div>
            <div class="btn-warning">
                <button type="button" class="tip text-color btnSiNo btn-pista" id="btnSi" onclick=getPista(urlPista,idPista)> Si </button>
                <button type="button" class="tip text-color btnSiNo btn-pista" id="btnNo" onclick=cerrarPista(id)> No </button>
            </div>
        </div>

        <div class="info-tip-show hide" id="show-tip1">PISTA 1</div>
        <div class="info-tip-show hide" id="show-tip2">PISTA 2</div>
        <div class="info-tip-show show" id="show-solution">SOLUCIÓN</div>
    `
})


// btnSolucion.onclick = function(){                   // ACCIONES DE SOLUCIÓN

//     pistaPanel.classList.remove('hide')
//     pistaPanel.classList.add('show')

//     urlPista = 'acertijo/actual/respuesta'
//     idPista = 'solucion'

//     pistaContent.innerHTML=`
//         <div class="bg-pista">
//             <div class="tip-warning">
//                 <h1>SOLUCIÓN</h1>
//                 <p>
//                     <br>
//                     Ver la solución, <b>te quitará todos los puntos en este acertijo</b> <br>
//                     ¿Estas seguro que quieres ver la solución?
//                 </p>
//             </div>
//             <div class="btn-warning">
//                 <button type="button" class="tip text-color btnSiNo btn-pista" id="btnSi" onclick=getPista(urlPista,idPista)> Si </button>
//                 <button type="button" class="tip text-color btnSiNo btn-pista" id="btnNo" onclick=cerrarPista(id)> No </button>
//             </div>
//         </div>

//         <div class="info-tip-show hide" id="show-tip1">PISTA 1</div>
//         <div class="info-tip-show hide" id="show-tip2">PISTA 2</div>
//         <div class="info-tip-show show" id="show-solution">SOLUCIÓN</div>
//     `
// }

function getPista(urlPista,idPista){                // OBTENER INFORMACIÓN DE BASE DE DATOS

    pistaPanel.classList.remove('show')
    pistaPanel.classList.add('hide')

    fetch(urlPista)
    .then(res => res.json())
    .then(data =>{
        cargarFeedback()
    })
}

function cerrarPista(idPista){                      //CERRAR PISTA

    pistaPanel.classList.remove('show')
    pistaPanel.classList.add('hide')

    if(idPista == "tip1"){
        verPista1 = true
    }

    if(idPista == "tip2"){
        verPista2 = true
    }

    if(idPista == "solucion"){
        cerrarAcertijo()
    }
}