var rankinglist = document.getElementById('ranking-list')
var rankingtable =  document.getElementById('ranking-table')
var btnlist = document.getElementById('btn-rankings')

var tableData = document.getElementById('ranking-data')



var url
var color
var name
var bg
var nro = 0
var bgNro = 0


document.getElementById('btn-return').onclick = function(){
    window.location="acertijo";
}

document.getElementById('btn-rankings').onclick = function(){

    btnlist.classList.remove('show')
    btnlist.classList.add('hide')
    window.location = "ranking"
}


document.getElementById('btn-gnral').onclick = function(){
    url = 'ranking/general'
    name = 'RANKING GENERAL'
    color = 'bg-a2'
    bg = 'bg-rankingA2.png'
    getRanking(url,name,color,bg)

}
if(typeof(document.getElementById('btn-a1')) != 'undefined' && document.getElementById('btn-a1') != null){
    document.getElementById('btn-a1').onclick = function(){
        url = 'ranking/acertijo/1'
        name = 'RANKING ACERTIJO 1'
        color = 'bg-a2'
        bg = 'bg-rankingA2.png'
        getRanking(url,name,color,bg)
}
}
if(typeof(document.getElementById('btn-a2')) != 'undefined' && document.getElementById('btn-a2') != null){

    document.getElementById('btn-a2').onclick = function(){
        url = 'ranking/acertijo/2'
        name = 'RANKING ACERTIJO 2'
        color = 'bg-a2'
        bg = 'bg-rankingA2.png'
        getRanking(url,name,color,bg)
    }
}
if(typeof(document.getElementById('btn-a3')) != 'undefined' && document.getElementById('btn-a3') != null){

    document.getElementById('btn-a3').onclick = function(){
        url = 'ranking/acertijo/3'
        name = 'RANKING ACERTIJO 3'
        color = 'bg-a2'
        bg = 'bg-rankingA2.png'
        getRanking(url,name,color,bg)
    }
}
if(typeof(document.getElementById('btn-a4')) != 'undefined' && document.getElementById('btn-a4') != null){

    document.getElementById('btn-a4').onclick = function(){
        url = 'ranking/acertijo/4'
        name = 'RANKING ACERTIJO 4'
        color = 'bg-a2'
        bg = 'bg-rankingA2.png'
        getRanking(url,name,color,bg)
    }
}
if(typeof(document.getElementById('btn-a5')) != 'undefined' && document.getElementById('btn-a5') != null){

    document.getElementById('btn-a5').onclick = function(){
        url = 'ranking/acertijo/5'
        name = 'RANKING ACERTIJO 5'
        color = 'bg-a2'
        bg = 'bg-rankingA2.png'
        getRanking(url,name,color,bg)
    }
}
if(typeof(document.getElementById('btn-a6')) != 'undefined' && document.getElementById('btn-a6') != null){

    document.getElementById('btn-a6').onclick = function(){
        url = 'ranking/acertijo/6'
        name = 'RANKING ACERTIJO 6'
        color = 'bg-a2'
        bg = 'bg-rankingA2.png'
        getRanking(url,name,color,bg)

    }    
}
if(typeof(document.getElementById('btn-a7')) != 'undefined' && document.getElementById('btn-a7') != null){

    document.getElementById('btn-a7').onclick = function(){
        url = 'ranking/acertijo/7'
        name = 'RANKING ACERTIJO 7'
        color = 'bg-a2'
        bg = 'bg-rankingA2.png'
        getRanking(url,name,color,bg)

    }    
}

function getRanking(url,name,color,bg){
    rankinglist.classList.remove('show')
    rankinglist.classList.add('hide')
    rankingtable.classList.remove('hide')
    rankingtable.classList.add('show')
    btnlist.classList.remove('hide')
    btnlist.classList.add('show')

    rankingtable.style.backgroundImage = `url(../assets/img/ranking/${bg})`
    document.getElementById('ranking-titulo').innerHTML=`${name}`

    fetch(url)
    .then(res => res.json())
    .then(data => {

        getData(data,color)

    })
}

function getData(data,color){

    tableData.innerHTML = ''
    for (let valor of data){

        if(valor.nombre){

            nro++

            if(bgNro === 5 ){
                bgNro = 1
            }else{
                bgNro++
            }

            var tiempo
            var min
            var sec

            if(valor.tiempo == 999999999){
                 min = '--'
                 sec = '--'
            }else{
                tiempo = parseInt(valor.tiempo)
                min = parseInt(tiempo/60)
                sec = tiempo - (min * 60)

                if(min<10){ min = `0${min}` }
                if(sec<10){sec = `0${sec}`}
            }



            tableData.innerHTML +=`
                <tr class="${color}${bgNro}">
                    <td class="table-name">${nro}. ${valor.nombre}</td>
                    <td class="table-info">${valor.puntos}</td>
                    <td class="table-info">${min}:${sec}</td>
                </tr>
            `
        }else{

        }

    }
}
