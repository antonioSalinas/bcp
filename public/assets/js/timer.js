var cronos
var tiempo
var cont

var minP1
var minP2
var pistaT1
var pistaT2
var tiempoInicio 
var horaServidor 
var tiempoTotal



function iniciarTimer(){                //LLAMADA DESDE EL BOTON GO O DESDE OBTENER ACERTIJO

    fetch('acertijo/actual')
    .then(res => res.json())
    .then(data =>{
        getDataTimer(data)
    })
}

function getDataTimer(data){            // OBTENEMOS EL TIEMPO INICIAL

    if (data.tiempo_inicio){

	    var h_inicio = data.tiempo_inicio.replace(' ','T')
        var h_servidor = data.hora_servidor.replace(' ','T')
       	 tiempoInicio = new Date(h_inicio)
         horaServidor = new Date(h_servidor)
        // tiempo = (data.tiempo*60)-parseInt((horaServidor - tiempoInicio)/1000)
      
        tiempo = (data.tiempo*60)-parseInt((horaServidor.getTime() - tiempoInicio.getTime())/1000)

    }else{
        tiempo = (data.tiempo*60) 
    }

    timer(data,tiempo)

}

var tiempoglobal

function timer(data,tiempo){            // CRONÓMETRO    
    
    tiempoglobal=tiempo

    cronos = setInterval(function() {

        if (document.getElementById('feedback-content').classList[1]=='show'){
            stopTimer(cronos)
        }
        else{
            tiempoglobal = tiempoglobal-1
            contTimer(data,tiempoglobal)
        }

    }, 1000);

}

function contTimer(data, tiempo){


    if(tiempo % 3 == 0){
        actualizar()        
    }
    if (data.tiempo == 0)
        {
            console.log('acato')
            document.getElementById('timer').innerHTML = 'Ilimitado'
            document.getElementById('tip1').innerText=`PISTA 1`
            document.getElementById('tip1').classList.remove('tip-normal')
            document.getElementById('tip1').classList.add('tip-active')
            parentActive = document.getElementById('tip1').parentNode
            parentActive.classList.add('active')
            parentActive = document.getElementById('tip2').parentNode
            parentActive.classList.add('active')
            document.getElementById('tip2').innerText=`PISTA 2`
            document.getElementById('tip2').classList.remove('tip-normal')
            document.getElementById('tip2').classList.add('tip-active')
            iniciarPista1()
            iniciarPista2()
 

        }

    if(tiempo > 0 ){

        min = parseInt(tiempo / 60)
        sec = parseInt(tiempo - (60 * min))
        
        if(min<10){ min = `0${min}` }
        if(sec<10){sec = `0${sec}`}
        document.getElementById('timer').innerHTML = `${min}:${sec} `

        if (tiempo < (data.tiempo - data.pista1t)*60){      //desbloque bnt pista 1 en X desde bd minutos    
            document.getElementById('tip1').innerText=`PISTA 1`
            document.getElementById('tip1').classList.remove('tip-normal')
            document.getElementById('tip1').classList.add('tip-active')
            // Jaraya 15/Dic/21 agregado texto de que no quita puntos al botón
            parentActive = document.getElementById('tip1').parentNode
            parentActive.classList.add('active')
            iniciarPista1()
        }else{
            minP1 = Math.abs(data.tiempo - data.pista1t - min)
            document.getElementById('tip1').innerText=`PISTA 1 en: ${minP1}:${sec}`
        }

        if (tiempo < (data.tiempo - data.pista2t)*60){       //desbloque bnt pista 2 en X desde bd minutos
            document.getElementById('tip2').innerText=`PISTA 2`
            document.getElementById('tip2').classList.remove('tip-normal')
            document.getElementById('tip2').classList.add('tip-active')

            // Jaraya 15/Dic/21 agregado texto de que no quita puntos al botón
            parentActive = document.getElementById('tip2').parentNode
            parentActive.classList.add('active')
            iniciarPista2()
        }else{
            minP2 =Math.abs(data.tiempo - data.pista2t - min) 
            
            document.getElementById('tip2').innerText=`PISTA 2 en: ${minP2}:${sec}`
        }

    }else{
        if (data.tiempo > 0)
        {
            stopTimer(cronos)
            timeOut(data)
        }
    }
}

function stopTimer(cronos){             // DETENER EL TIMER

    clearInterval(cronos)

}

function timeOut(data){                 // FUERA DE TIEMPO

    fetch('acertijo/actual/respuesta')       // VISITAMOS AL JSON
    .then(res => res.json())
    .then(data => {
    })


    document.getElementById('div-answer').innerHTML=`
        <div class="bg-answer-e">
            SE TERMINÓ EL TIEMPO
        </div>
    `

    setTimeout(function(){

        cargarFeedback()

    },1500)

}


function actualizar(){


    fetch('acertijo/actual')
    .then(res => res.json())
    .then(data =>{

        if (data.finalizado){
            location.reload();
        }

    })

}
