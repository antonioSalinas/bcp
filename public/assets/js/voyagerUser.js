var columnas
var selectData
var grupos
var usuarios
window.onload = function(){        
    fillCombo()
    getData(-1)
        
}
var notShow =[]
var visibles = []
getData = (opc) =>{
    var tbody = document.getElementById('tbody')
    tbody.innerHTML = ''
    var myInit = { method: 'POST' }
    var myRequest = new Request('/api/getAllFields', myInit)
    
    fetch(myRequest)
    .then(res => res.json())
    .then(data =>{
        
        columnas=data
        var myInit = { method: 'POST' }
        var myRequest = new Request('/api/getGrupos', myInit)
        fetch(myRequest)
        .then(res => res.json())
        .then(data =>{
            selectData=data
            grupos=data
            notShow =['email_verified_at','api_token','remember_token','settings','created_at','updated_at','enabled','is_first_time','avatar','logged']

            var init
            if(opc != -1){
                url = '/api/getUsersFiltered'
                var formData = new FormData();   
                    formData.append('equipo_id', opc);
                    init ={
                        method: 'POST',
                        body: formData
                    }
                    
            }
            else{
                url = '/api/getUsers'
                init = { method: 'POST' }
            }
            var myRequest = new Request(url,init)   
            fetch(myRequest)
            .then(res => res.json())
            .then(data =>{
                
                usuarios = data
                usuarios.forEach(usuario => { 
                    var form = document.createElement('form')
                     form.id = `form_${usuario.id}`
                     form.method = 'GET';
                     var table = document.createElement('table')
                     table.id = `usuario_${usuario.id}`
                     table.classList.add('table')
                     table.classList.add('table-hover')
                     table.classList.add('usuariosTable')
                     table.classList.add('no-footer')
                     table.setAttribute('role','grid')
                     table.setAttribute('describedby','usuariosTable_info')
                     table.setAttribute("cellpadding","10")

                     var row = document.createElement('tr')
                     row.setAttribute('role','row')
                     row.classList.add('odd')
                     row.id = `row_${usuario.id}`
                     var column_1
                     var column_2
                     var column_3
                     var column_4
                     var column_5
                     var column_6
                     var column_7
                     columnas.forEach(columna => {
                        if(!(notShow.includes(columna.Field))){
                            
                            if(columna.Field === "grupo_id")
                            {
                                column_1 = document.createElement('td')
                                var small_1 = document.createElement('small')
                                small_1.id = `${columna.Field}_${usuario.id}`
                                small_1.classList.add('form-text')    
                                small_1.classList.add('text-muted')
                                small_1.innerHTML = 'Grupo'  
                                column_1.appendChild(small_1)
                                salto = document.createElement('br')
                                column_1.appendChild(salto)
                                var select_1 = document.createElement('select')
                                select_1.id =`grupo_id_${usuario.id}`
                                select_1.name =`grupo_id_${usuario.id}`
                                select_1.classList.add('form','form-control','form-control-sm')
                                var dmy_opt = document.createElement('option')
                                dmy_opt.value = -1
                                dmy_opt.setAttribute('selected','selected')
                                dmy_opt.setAttribute('disabled','disabled')
                                dmy_opt.innerHTML= 'Asignar...'
                                var seleccionado = 0
                                grupos.forEach(grupo => {
                                    var opt = document.createElement('option')
                                    if(grupo.id == usuario.grupo_id)
                                    {
                                        opt.value = grupo.id
                                        opt.setAttribute('selected','selected')
                                        opt.innerHTML = grupo.nombre
                                        seleccionado = 1
                                        
                                    }else{
                                        opt.value = grupo.id
                                        opt.innerHTML = grupo.nombre
                                    }
                                    select_1.appendChild(opt)
                                })
                                if(seleccionado == 0)
                                {
                                    opt = document.createElement('option')
                                    opt.value = -1
                                    opt.setAttribute('selected','selected')
                                    opt.innerHTML = 'SIN GRUPO'
                                    seleccionado = 1
                                    
                                }else{
                                    opt = document.createElement('option')
                                    opt.value = -1
                                    opt.innerHTML = 'SIN GRUPO'
                                }
                                select_1.appendChild(opt)
                                column_1.appendChild(select_1)
                                row.appendChild(column_1)
           
                            }else   if(columna.Field === "role_id")  {
                                        column_2 = document.createElement('td')
                                        var small_2 = document.createElement('small')
                                        small_2.id = `${columna.Field}_${usuario.id}`
                                        small_2.classList.add('form-text')    
                                        small_2.classList.add('text-muted')
                                        small_2.innerHTML = 'Rol Usuario'  
                                        column_2.appendChild(small_2)
                                        var select_2 = document.createElement('select')
                                        select_2.classList.add('form','form-control','form-control-sm')
                                        select_2.id = `role_id_${usuario.id}`
                                        select_2.name = `role_id_${usuario.id}`
                                        if(usuario.role_id == 1){  
                                            opt = document.createElement('option')
                                            opt.value = 1
                                            opt.setAttribute('selected','selected')
                                            opt.innerHTML = 'Administrador'
                                            select_2.appendChild(opt)
                                            opt = document.createElement('option')
                                            opt.value = 2
                                            opt.innerHTML = 'Usuario Normal'
                                            select_2.appendChild(opt)
                                            column_2.appendChild(select_2)
                                        }else{
                                            opt = document.createElement('option')
                                            opt.value = 1
                                            opt.innerHTML = 'Administrador'
                                            select_2.appendChild(opt)
                                            opt = document.createElement('option')
                                            opt.value = 2
                                            opt.innerHTML = 'Usuario Normal'
                                            opt.setAttribute('selected','selected')
                                            select_2.appendChild(opt)
                                            column_2.appendChild(select_2)
                                            
                                        }
                                        row.appendChild(column_2)
                                        }else if(columna.Field === "id"){
                                            column_3 = document.createElement('td')
                                            var input = document.createElement('input')
                                            input.type = 'checkbox'
                                            input.id = `checkbox_${usuario.id}`
                                            input.name = `checkbox_${usuario.id}`
                                            input.classList.add('form-check-input')
                                            input.value = usuario.id
                                            if(usuario.role_id == 1) {
                                                input.setAttribute('disabled','disabled')
                                            }  
                                            column_3.appendChild(input)
                                            label = document.createElement('label')
                                            label.classList.add('sr-only')
                                            label.setAttribute('for', `checkbox_${usuario.id}`)
                                            label.innerHTML = usuario.id
                                            column_3.appendChild(label)
                                            input = document.createElement('input')
                                            input.type = 'text'
                                            input.id = 'id'
                                            input.name = 'id'
                                            input.value = usuario.id
                                            input.setAttribute('hidden','hidden')
                                            column_3.appendChild(input)
                                            row.prepend(column_3)

                                        }
                                        else if(columna.Field === "password"){
                                            column_4 = document.createElement('td')
                                            var small_4 = document.createElement('small')
                                            small_4.id = `${columna.Field}_${usuario.id}`
                                            small_4.classList.add('form-text')    
                                            small_4.classList.add('text-muted')
                                            small_4.innerHTML = columna.Field  
                                            column_4.appendChild(small_4)
                                            br = document.createElement('br')
                                            column_4.appendChild(br)
                                            var input = document.createElement('input')
                                            input.type = 'password'
                                            input.classList.add('form','form-control','form-control-sm')
                                            input.id = `password_${usuario.id}`
                                            input.name = `password_${usuario.id}`
                                            input.value = '*************************'
                                            column_4.appendChild(input)
                                            row.appendChild(column_4)
            
                                        }                    
                                        else{
                                          
                                            column_5 = document.createElement('td')
                                            var small_5 = document.createElement('small')
                                            small_5.id = `${columna.Field}_${usuario.id}`
                                            small_5.classList.add('form-text')    
                                            small_5.classList.add('text-muted')
                                            small_5.innerHTML = columna.Field  
                                            column_5.appendChild(small_5)
                                            var input = document.createElement('input')
                                            input.type = 'text'
                                            input.classList.add('form','form-control','form-control-sm')
                                            input.id = `${columna.Field}_${usuario.id}`
                                            input.name = `${columna.Field}_${usuario.id}`
                                            input.value = usuario[columna.Field]
                                            column_5.appendChild(input)
                                            row.appendChild(column_5)
                                            
                                        }   
                        }
                     })
                    column_6 = document.createElement('td')
                    var small_6 = document.createElement('small')
                    small_6.classList.add('form-text')    
                    small_6.classList.add('text-muted')
                    small_6.innerHTML = '&nbsp;<br>' 
                    column_6.appendChild(small_6)
                    badge_save= document.createElement('badge')
                    badge_save.classList.add('btn')
                    badge_save.classList.add('btn-primary')
                    badge_save.classList.add('save')
                    badge_save.value = 'Guardar'
                    badge_save.onclick = () => saveUser(`form_${usuario.id}`)
                    if(usuario.role_id == 1) {
                        badge_save.onclick = () => alert('no permitido')
                        badge_save.setAttribute('disabled','disabled')

                    }

                    badge_save.innerHTML = 'Guardar'
                    column_6.appendChild(badge_save)
                    column_7 = document.createElement('td')
                    var small_7 = document.createElement('small')
                    small_7.classList.add('form-text')    
                    small_7.classList.add('text-muted')
                    small_7.innerHTML = '&nbsp;<br>' 
                    column_7.appendChild(small_7)
                    badge_del= document.createElement('badge')
                    badge_del.classList.add('btn')
                    badge_del.classList.add('btn-danger')
                    badge_del.value = 'Borrar'
                    badge_del.onclick = () => deleteUser(usuario.id)
                    if(usuario.role_id == 1) {
                        badge_del.setAttribute('disabled','disabled')
                        badge_del.onclick = () => alert('no permitido')
                    }
                    badge_del.innerHTML = 'Borrar'
                    column_7.appendChild(badge_del)
                    row.appendChild(column_6)
                    row.appendChild(column_7) 
                    table.appendChild(row)
                    form.appendChild(table)
                    tbody = document.getElementById('tbody')
                    tbody.appendChild(form) 
                    
                 }) 
                
                
                
                
               
                notShow.push("id");
                var form = document.createElement('form')
                form.id =  'user_form'
                form.method = 'GET'
                var table = document.createElement('table')
                columnas.forEach(columna => {
                    if(!(notShow.includes(columna.Field))){
                        if(columna.Field === "grupo_id")
                        {
                            var tr = document.createElement('tr')
                            var tdName = document.createElement('td')
                            tdName.style.width = '100px'
                            var label = document.createElement('label')
                            label.setAttribute('for','grupo_id')
                            label.innerHTML = 'Grupo'
                            tdName.appendChild(label)
                            tr.appendChild(tdName)
                            var tdValue = document.createElement('td')
                            tdValue.style.width = '200px'
                            select = document.createElement('select')
                            select.classList.add('form-control')
                            select.id = 'grupo_id'
                            select.name = 'grupo_id'
                            select.style.width = '200px'
                            var opt = document.createElement('option')
                            opt.value = -1
                            opt.innerHTML = 'Asignar ...'
                            opt.setAttribute('selected','selected')
                            opt.setAttribute('disabled','disabled')
                            select.appendChild(opt)
                            grupos.forEach(grupo => {
                               var opt = document.createElement('option')
                               opt.value = grupo.id
                               opt.innerHTML = grupo.nombre
                               select.appendChild(opt)
                            });
                            tdValue.appendChild(select)
                            tr.appendChild(tdValue)
                            table.appendChild(tr)
                        }else   if(columna.Field === "role_id")  {
                            var tr = document.createElement('tr')
                            var tdName = document.createElement('td')
                            tdName.style.width = '100px'
                            var label = document.createElement('label')
                            label.setAttribute('for','role_id')
                            label.innerHTML = 'Rol ID'
                            tdName.appendChild(label)
                            tr.appendChild(tdName)
                            var td = document.createElement('td')
                            td.style.width = '200px'
                            var select = document.createElement('select')
                            select.classList.add('form-control')
                            select.id = 'role_id'
                            select.name = 'role_id'
                            select.style.width = '200px'
                            var opt = document.createElement('option')
                            opt.value = 1
                            opt.innerHTML = 'Administrador'
                            select.appendChild(opt)
                            opt = document.createElement('option')
                            opt.value = 2
                            opt.innerHTML = 'Usuario Normal'
                            opt.setAttribute('selected','selected')
                            select.appendChild(opt)
                            td.appendChild(select)
                            tr.appendChild(td)
                            table.appendChild(tr)
                        }
                        else if(columna.Field === "password"){
                            var tr = document.createElement('tr')
                            var tdName = document.createElement('td')
                            tdName.style.width = '100px'
                            var label = document.createElement('label')
                            label.setAttribute('for','password')
                            label.innerHTML = 'Password'
                            tdName.appendChild(label)
                            tr.appendChild(tdName)
                            var td = document.createElement('td')
                            td.style.width = '200px'
                            var input = document.createElement('input')
                            input.classList.add('form-control')
                            input.setAttribute('type','password')
                            input.style.width = '200px'
                            input.id = 'password'
                            input.name = 'password'
                            input.value = ""
                            td.appendChild(input)
                            tr.appendChild(td)
                            table.appendChild(tr)
                            var tr = document.createElement('tr')
                            var tdName = document.createElement('td')
                            tdName.style.width = '100px'
                            var label = document.createElement('label')
                            label.setAttribute('for','conf_password')
                            label.innerHTML = 'Confirmar Password'
                            tdName.appendChild(label)
                            tr.appendChild(tdName)
                            var td = document.createElement('td')
                            td.style.width = '200px'
                            var input = document.createElement('input')
                            input.classList.add('form-control')
                            input.setAttribute('type','password')
                            input.style.width = '200px'
                            input.id = 'conf_password'
                            input.name = 'conf_password'
                            input.value = ""
                            td.appendChild(input)
                            tr.appendChild(td)
                            table.appendChild(tr)

                        }
                        
                        else{
                            var tr = document.createElement('tr')
                            var td = document.createElement('td')
                            td.style.width = '100px'
                            var label = document.createElement('label')
                            label.setAttribute('for',columna.Field)
                            label.innerHTML = columna.Field
                            td.appendChild(label)
                            tr.appendChild(td)
                            var td = document.createElement('td')
                            td.style.width = '200px'
                            var input = document.createElement('input')
                            input.classList.add('form-control')
                            input.setAttribute('type','text')
                            input.style.width = '200px'
                            input.id = columna.Field
                            input.name = columna.Field
                            input.value = ""
                            td.appendChild(input)
                            tr.appendChild(td)
                            table.appendChild(tr)
            
                        }   
                    }
                 })
                 form.appendChild(table)
                 var badge = document.createElement('badge')
                 badge.classList.add('btn', 'btn-primary','save') 
                 badge.value = 'Guardar'
                 badge.onclick = () => newUser(form)
                 badge.innerHTML = 'Guardar'
                 form.appendChild(badge)
                 document.getElementById('form_newuser').appendChild(form)    
                 var selCol = document.createElement('select')
                 selCol.id = 'columnas'
                 selCol.name = 'columnas'
                 selCol.classList.add('form','form-control')
                 notShow.push("id","cellphone","username", "email","password", "role_id","grupo_id" );
                 columnas.forEach(col => {
                    if(!(notShow.includes(col.Field))){
                        var opt = document.createElement('option')
                        opt.innerHTML = col.Field
                        selCol.appendChild(opt)
                    }
                 }); 
                 document.getElementById('selCol').appendChild(selCol)    
            
            
            
            })   


        })

    })
    

    url= '/api/getGruposFiltered'
    data = { equipo_id : opc}
    init = {
        method: 'POST', 
        body: JSON.stringify(data), 
        headers:{
          'Content-Type': 'application/json'
        }
    }
    fetch(url,init)
    .then(response => response.json())
    .then(data => {
        document.getElementById('comboGrupos') != undefined ? document.getElementById('comboGrupos').remove():null
        var comboGrupos = document.createElement('select')
        comboGrupos.id = 'comboGrupos'
        comboGrupos.onchange = () => filtrar(comboGrupos.value)
        comboGrupos.classList.add('form')
        comboGrupos.classList.add('form-control','form-control-sm')
        var option = document.createElement('option')
        option.value= -1
        option.innerHTML = 'Seleccionar Grupo'
        comboGrupos.appendChild(option)
        document.getElementById('combo_equipos').appendChild(comboGrupos)
        data.forEach(element => {
            var opt = document.createElement('option')
            opt.value = element.id
            opt.innerHTML = element.nombre
            comboGrupos.appendChild(opt)
        });
      
    });

    }



saveUser = function (form) {
  
    var formData = new FormData(document.getElementById(form));
    url = '/api/saveUser'
    init = {
        method: "POST",
        body: formData
      }              
    fetch(url,init)
    .then(res => res.json())
    .then(data =>{
        alert(data.message)
    })              
   
}
nuevaColumna = function(){
    var columna = prompt("Nueva Columna", "");
    if (columna == null || columna == "") {
    txt = "Cancelado.";
    } else {
        var myRequest = new Request('/api/createUserField?columna='+columna)                
        fetch(myRequest)
        .then(res => res.json())
        .then(data =>{
            alert(data.message)
            location.reload()
        })
        
    }
}
newUser = function(form){
    if(document.getElementById('password').value != document.getElementById('conf_password').value){
        alert('Pasword no coincide')
        return
    }
    formData = new FormData(form)
    var url = '/api/createUser'
    var init = {
        method : 'POST',
        body : formData
    }               
    fetch(url,init)
    .then(res => res.json())
    .then(data =>{
        alert(data.message)
        if(data.error == 0){
            location.reload()
        }
        
    })
    

}
deleteUser= function(id){
    var myRequest = new Request('/api/deleteUser?id='+id)                
    fetch(myRequest)
    .then(res => res.json())
    .then(data =>{
        alert(data.message)
        tbody = document.getElementById('tbody')
        form = document.getElementById(`form_${id}`)
        tbody.removeChild(form) 
    })
  
}
deleteColumn = function(selector){
    var myRequest = new Request('/api/deleteColumn?column='+selector)                
    fetch(myRequest)
    .then(res => res.json())
    .then(data =>{
        alert(data.message)
        location.reload()
    })
    
}
getLayout = function(){
    var myRequest = new Request('/api/getAllFieldsExcel')                
    fetch(myRequest)
    .then(res => res.blob())
    .then(blob =>{
        var url = window.URL.createObjectURL(blob);
        var a = document.createElement('a');
        a.href = url;
        a.download = "plantilla.xlsx";
        document.body.appendChild(a); 
        a.click();    
        a.remove();  
    })
}
deleteAll = () => {
    const todelete = document.querySelectorAll('input[type="checkbox"]')
    var topass =[]
    todelete.forEach(element => {
    if (element.checked){
        topass.push(element.value)
    }   
    });
    if(confirm('Va a eliminar usuarios')){
        var formData = new FormData();
        formData.append('ids',topass)
        url = '/api/deleteAll'
        init = {
            method: "POST",
            body: formData
        }               
        fetch(url,init)
        .then(res => res.json())
        .then(data =>{
            (data.message)
            location.reload()
        })
    
    }
    
}

fillCombo = () => {
    var myRequest = new Request('/api/getAllEquipos')                
    fetch(myRequest)
    .then(res => res.json())
    .then(data =>{
        var combo = document.createElement('select')
        combo.id = 'combo_filter'
        combo.onchange = (event) => getData(event.target.value)
        combo.classList.add('form')
        combo.classList.add('form-control','form-control-sm')
        var dmyopt = document.createElement('option')
        dmyopt.value = -1
        dmyopt.innerHTML = 'TODOS'
        combo.appendChild(dmyopt)
        dmyopt = document.createElement('option')
        dmyopt.value = 0
        dmyopt.innerHTML = 'SIN EQUIPO'
        combo.appendChild(dmyopt)
        data.forEach(element => {
            var opt = document.createElement('option')
            opt.value = parseInt(element.id)
            opt.innerHTML = element.descripcion
            combo.appendChild(opt)
        });
        document.getElementById('combo_equipos').prepend(combo)
    
        

    })
}
filtrar = (valor) => {
    selects = document.querySelectorAll('select')
    tables =  document.querySelectorAll('table')
    tables.forEach(table => {
        table.style.display = 'none'
    });
    selects.forEach(select => {
        if((select.value == valor)&&(select.name.includes('grupo_id_'))){
            splitted = select.id.split('_')
                document.getElementById(`usuario_${splitted[2]}`).style.display = 'block'
        }
        
    });

}