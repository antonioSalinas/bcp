<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'last_name' => 'Apellido',
    'cellphone' => 'Celular',
    'username' => 'Usuario',
    'enabled' => 'Habilitado',
    'username_and_password_are_required' => 'Username and password are required',
    'wrong_username_or_password' => 'Wrong username or password',
    'you_do_not_have_group_enabled' => 'You don\'t have group enabled',

];
