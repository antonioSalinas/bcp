<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'last_name' => 'Apellido',
    'cellphone' => 'Celular',
    'username' => 'Usuario',
    'enabled' => 'Habilitado',
    'username_and_password_are_required' => 'Se requiere nombre de usuario y contraseña',
    'wrong_username_or_password' => 'Usuario o contraseña incorrectos',
    'you_do_not_have_group_enabled' => 'No tienes grupo activado',
    'is_first_time' => 'Primera vez de acceso',
    'send_information' => 'Enviar información',
    'import' => 'Importar',
    'generate_groups' => 'Generar grupos',
    'export_ranking' => 'Exportar ranking',

];
