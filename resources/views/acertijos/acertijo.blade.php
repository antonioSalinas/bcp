@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/css/mainAcertijo.css')}}">
    <!-- <link rel="stylesheet" href="{{asset('assets/css/themeOrange.css')}}"> -->
    <link rel="stylesheet" id="theme">
@endsection

@section('content')
    <div class="external-content hide zIndex1" id="external-content"></div>

    <div class="container">
        
        <div id="bg-der"></div>
        <div id="bg-izq"></div>

        <div class="acertijo-body">
            <div class="logo-section">
                <div id="logo-samay"></div>
            </div>

            <div class="option-section">
                <div class="option-content">
                    <div class="bg-btn-options">
                        <button class="btn-option" id="btn-ranking">
                            Ranking
                        </button>
                    </div>
			<br>
		    <div class="bg-btn-options">
                        <button class="btn-option" id="btn-team">
                            Tu equipo
                        </button>
                    </div>
                	<br>
                	 <div class="bg-btn-options">
                       <button class="btn-option" id="btn-logout">
                            Salir
                        </button>
		         </div> 
 	  		

                </div>
            </div>

            <div class="acertijo-section">

                <div class="acertijo-header text-color" id="acertijo-header">
                    <div class="img-titleA " id="img-title"></div>
                    <h1 id="acertijo-title"></h1>
                </div>


                <div class="acertijo-content">
                    <div id="acertijo-info" >
                        <!-- LLENAR INFORMACIÓN DE ACERTIJO-->
                    </div>
                    <iframe id="iframe-download" style="display:none;"></iframe>
                </div>

                <div class="text-color-default hide" id="feedback-content">
                    <div id="info-solucion">
                        <!-- INFORMACIÓN DE SOLUCIÓN -->
                    </div>
                    <div id="info-feedback">
                        <!-- LLENAR INFO DE FEEDBACK -->
                    </div>
                    <div id="btnFeedback">
                        <!-- <button id="btn-feedback" type="button">SIGUIENTE DESAFÍO</button> -->
                    </div>
                </div>

                <div  class="show" id="acertijo-form">
                    <form class="form-content" id="form-acertijo" autocomplete="off">
                        <div class="form-division">
                            <div id="div-answer">
                                <div class="bg-answer">
                                    <div id="bg-clave"></div>
                                    <input type="text" placeholder="" name="respuesta" id="clave" class="input text-color border-color" autocomplete="off" onKeyUp="checkLen()">
                                    <button id="btn-comprobar" type="submit" >Comprobar</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-division">
                            <div class="div-tip">



                                <div class="bg-tip" id="bg-tip1">
                                    <button class="tip text-color tip-normal" type="button" id="tip1" disabled></button>
                                    <!-- <span class="warning-tip text-color hide" id="warning-tip1">
                                        Utilizar esta pista te restará 20 puntos
                                    </span> -->
                                </div>

                                <div class="space"></div>

                                <div class="bg-tip"  id="bg-tip2">
                                    <button class="tip text-color tip-normal" type="button" id="tip2" disabled></button>
                                    <!-- <span class="warning-tip text-color hide" id="warning-tip2">
                                        Utilizar esta pista te restará 40 puntos
                                    </span> -->
                                </div>

                                <div class="space"></div>

                                <div class="bg-solution"  id="bg-solution">
                                    <button class="solution text-color sol-normal" type="button" id="solution"  >SOLUCIÓN</button>
                                    <span class="warning-solution text-color hide" id="warning-solution">
                                        <b>¡CUIDADO!</b><br>
                                        Utilizar esta pista te restará todos los puntos
                                    </span>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>



    <div class="external-content bg-color hide" id="tip-panel">
        <div class="container">
            <div class="tip-content" id="tip-content">
                <!-- CONTENIDO DE PISTAS -->
            </div>
        </div>
    </div>

    <div class="external-content bg-options hide" id="user-panel">
        <div class="container">
            <div class="panel-content">
                <div class="panel-info">
                    <span class="tip-close" id="closeTeam"></span>
                    <form id="formTeam" action ="{{ action('GrupoController@cambiarNombre')}}">
                        <input class="input" type="text" id="groupName" placeholder="NOMBRE DE GRUPO" name="nombre">
                        <button id="btnGroupName" type="submit">GUARDAR</button>
                        <input class="input" type="text" id="userId" value="{{ Auth::user()->id }}" name="userId">
                    </form>
                    <ul id="team-list">
                        <!-- CONTENIDO DE EQUIPO -->
                    </ul>
                </div>
                <div class="option-active">
                    <div class="option-team">Tu equipo</div>
                </div>
            </div>
        </div>
    </div>

    <div class="external-content bg-options hide" id="rules-panel">
        <div class="container">
            <div class="panel-content">
                <div class="panel-info">
                    <span class="tip-close" id="closeRules"></span>    
                    <h1>DESAFÍO EXPLORA</h1>
                    <div class="info-rules">
                        <p>
                            <span class="icon-rules"></span>
                            Es un juego de resolución de acertijos colaborativo, con pistas que les
                            permitirán encontrar la clave para avanzar en el juego. Utilicen compartir
                            pantalla y toda herramienta que les sea útil para mejorar su trabajo en
                            equipo.
                        </p>
                        <p>
                            <span class="icon-rules"></span>
                            Cada desafío resuelto les da puntaje.  Según el tiempo que demoren será
                            su puntaje por cada desafío, que va de 0 puntos a 600 puntos; mientras
                            menos demoren más puntaje obtendrán.
                        </p>
                        <p>
                            <span class="icon-rules"></span>
                            Para partir un desafío deben hacer clic en “GO” en la pantalla de inicio de
                            cada desafío, al hacerlo comenzará el tiempo del desafío. El primero del
                            equipo que haga clic a “GO” iniciará el contador, así que deben coordinar
                            entre ustedes para partir.
                        </p>
                        <p>
                            <span class="icon-rules"></span>
                            Dentro del desafío hay una pestaña Descargar Material, donde podrán
                            descargar material necesario para resolver el desafío. El material
                            habitualmente es distinto para cada miembro del equipo.
                        </p>
                        <p>
                            <span class="icon-rules"></span>
                            Hay pistas que les ayudarán, parten bloqueadas y se activan con el tiempo.
                            Recuerden revisarlas y usarlas porque les permitirán avanzar más rápido.
                            El botón solución muestra la solución del acertijo, pero CUIDADO, esto les
                            quitará todo el puntaje del desafío.
                        </p>
                        <p>
                            <span class="icon-rules"></span>
                            Una vez crean que tienen la respuesta a un desafío, pongan la respuesta y
                            comprueben si es correcta. Pueden intentar varias veces, no se descuentan
                            puntos por respuesta errada. Si es correcta recibirán el feedback y podrán
                            pasar al siguiente desafío.
                        </p>
                    </div>
                </div>
                <div class="option-active">
                    <div class="option-rules">Reglas</div>
                </div>
            </div>
        </div>
    </div>

    <div class="external-content hide" id="video-panel">
        <div class="container">
            <div id="video-content">
                <span class="tip-close" id="closeVideo"></span>     
                <!-- <video id="videoFin" src="../public/assets/media/samay-video.mov"  width="960" height="540" controls></video> -->
                <video id="videoFin" src="../assets/media/samay-video.mov" width="960" height="540" controls></video>
            </div>
        </div>
    </div>

    <!-- SCRIPTS -->

    <script src="{{asset('assets/js/obtenerAcertijo.js')}}"></script>
    <script src="{{asset('assets/js/playMedia.js')}}"></script>
    <script src="{{asset('assets/js/timer.js')}}"></script>
    <script src="{{asset('assets/js/check.js')}}"></script>
    <script src="{{asset('assets/js/pistas.js')}}"></script>
    <script src="{{asset('assets/js/options.js')}}"></script>
    <script src="{{asset('assets/js/feedback.js')}}"></script>
@endsection
