@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/css/index.css')}}">
@endsection

@section('content')
    <div class="screen">
        <div class="container bg-index" >
            <form id="form-index">

                <div id="logo-index"></div>

                <div class="form-content">
                    <div class="bg-index-input">
                        <input class="input" type="text" name="username" placeholder="Usuario">
                    </div>
                </div>

                <div class="form-content">
                    <div class="bg-index-input">
                        <input class="input" type="password" name="password" placeholder="Contraseña">
                    </div>
                </div>

                <div class="form-content" id="btn-login">
                    <div id="bg-index-button">
                        <button id="index-button" type="submit"> INGRESAR</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script src="{{asset('assets/js/login.js')}}"></script>
@endsection
