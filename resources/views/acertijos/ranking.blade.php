@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/css/mainAcertijo.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/ranking.css')}}">
@endsection

@section('content')
    <div class="container">
        <div class="ranking-body">

            <div id="ranking-content">
                <div class='show' id="ranking-list">
                    <div class="logo-section">
                        <div id="logo-index"></div>
                    </div>

                    <div class="ranking-section">
                        <div class="ranking-index">
                            <button class="ranking" id="btn-gnral" type="button">RANKING GENERAL</button>
                            @php $i=1 @endphp
                            @foreach($acertijo as $acert)
                                 <button class="ranking" id="btn-a{{$i}}" type="button">Ranking Acertijo {{$i}}</button>
                                @php $i++  @endphp
                            @endforeach
                            <!-- <button class="ranking" id="btn-gnral" type="button">RANKING GENERAL</button>
                            <button class="ranking" id="btn-a1" type="button">Ranking Acertijo 1</button>
                            <button class="ranking" id="btn-a2" type="button">Ranking Acertijo 2</button>
                            <button class="ranking" id="btn-a3" type="button">Ranking Acertijo 3</button>
                            <button class="ranking" id="btn-a4" type="button">Ranking Acertijo 4</button>
                            <button class="ranking" id="btn-a5" type="button">Ranking Acertijo 5</button>
                            <button class="ranking" id="btn-a6" type="button">Ranking Acertijo 6</button> -->

                        </div>
                    </div>
                </div>

                <div class="hide" id="ranking-table">
                    <table>
                        <thead>
                        <tr>
                            <th id="ranking-titulo" class="table-name"></th>
                            <th class="table-info">PUNTOS</th>
                            <th class="table-info">TIEMPO</th>
                        </tr>
                        </thead>
                        <tbody id="ranking-data">

                        </tbody>

                    </table>
                </div>


            </div>


            <div class="option-section">
                <div class="option-content">
                    <div class="bg-btn-optionsRkn">
                        <button class="btn-option" id="btn-return">
                            Volver al Juego
                        </button>
                    </div>
			<br>
                    <div class="bg-btn-optionsRkn hide" id="btn-rankings">
                        <button class="btn-option">
                            Ver otros ranking
                        </button>
                    </div>
                    <div class="bg-btn-options hide">
                        <button class="btn-option" id="btn-rules">
                            Reglas
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- SCRIPTS -->
    <script src="{{asset('assets/js/ranking.js')}}"></script>
@endsection
