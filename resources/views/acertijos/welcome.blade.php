@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/css/index.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/welcome.css')}}">
@endsection

@section('content')

    <div class="screen">
        <div class="container bg-index">
            <div id="bg-welcome">
                <div>
                    <form id="form-welcome">
                        <div id="welcome-title-content">

                            <div id="welcome-title" >
				                <br>
                                <h4></h4>
                                <br><br>
                                <p>
                				El Inca, preocupado por mantener el orden sobre su territorio, 
                				 sabía que pronto necesitaría crear caminos que unieran a todos los pueblos;
                				 así que decide enviar a Quilla y a Inti para que tracen el camino y marquen los puntos más importantes, 
                				que permitirían el desarrollo y comunicación de todos los pueblos.
                                                </p>
                  				<p>
                				Ayuda a Quilla y a Inti en esta aventura y descubre los valores más
                				 importantes que necesitarán para superar este desafío.
                                                </p>
                                            </div>
                                            
                                        </div>
                        		<div id="btn-welcome" onclick="inicializar()">EMPEZAR EL JUEGO</div>                       


                    </form>

                </div>

           </div>
        </div>
    </div>
    <script>
        var logo_url = "{{asset('assets/img/logo-samay.png')}}";
    </script>
    <script src="{{asset('assets/js/welcome.js')}}"></script>
@endsection
