<table>
    <thead>
    <tr>
        <th>Grupos</th>
        <th>Fecha creación</th>
        <th>Lugar en el ranking</th>
        <th>Puntaje</th>
        <th>Tiempo total</th>
        <th>Tiempo D1</th>
        <th>Puntaje D1</th>
        <th>Tiempo D2</th>
        <th>Puntaje D2</th>
        <th>Tiempo D3</th>
        <th>Puntaje D3</th>
        <th>Tiempo D4</th>
        <th>Puntaje D4</th>
        <th>Tiempo D5</th>
        <th>Puntaje D5</th>
        <th>Tiempo D6</th>
        <th>Puntaje D6</th>
    </tr>
    </thead>
    <tbody>
    @php
    $pos = 0;
    @endphp
    @foreach($grupos as $grupo)
        <tr>
            <td>{{ $grupo->nombre }}</td>
            <td>{{ $grupo->created_at }}</td>
            <td>{{ ++$pos }}</td>
            <td>{{ $grupo->puntos }}</td>
            <td>{{ $grupo->tiempo }}</td>
            @php
                $acertijos = [];
                if($grupo->acertijos)
                    $acertijos = explode(',', $grupo->acertijos);

            @endphp
            @foreach($acertijos as $acertijo)
                @php
                    list($puntuacion, $tiempo) = explode('/', $acertijo);
                @endphp
                <td>{{ $puntuacion }}</td>
                <td>{{ $tiempo }}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
