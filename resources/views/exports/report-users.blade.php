<table>
    <thead>
    <tr>
        <th>Usuario nombre</th>
        <th>Id</th>
        <th>Fecha creación</th>
        <th>Primer logeo</th>
        <th>Nombre Grupo</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{{$user->name}} {{$user->last_name}}</td>
            <td>{{$user->username}}</td>
            <td>{{$user->created_at}}</td>
            <td>{{$user->firstLoginAt()}}</td>
            @php
                $grupo_nombre = '';
                $grupo = $user->enabledGroup();
                if($grupo)
                    $grupo_nombre = $grupo->nombre;
            @endphp
            <td>{{$grupo_nombre}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
