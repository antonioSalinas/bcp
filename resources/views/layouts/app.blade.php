<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>CAJA AREQUIPA</title>

    <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
    @yield('styles')

    <link rel="icon" href="{{asset('assets/img/favicon.ico')}}" type="image/png"/>
<body>
@yield('content')
</body>
</html>
