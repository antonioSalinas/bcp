<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Auth::routes(['register' => false]);
Route::middleware(['logBeforeAfter'])->group(function () {
    Route::get('unauthorized',array('as'=>'login',function(){
        return response()->json(array('error'=>'unauthorized'));
    }));

    Route::post('login', 'AuthController@login');
    Route::get('logout', 'AuthController@logout');


    Route::group(['middleware' => ['auth:api']], function () {
        Route::get('acertijo/actual/pista/{index}', 'AcertijoController@pistaActual');
        Route::get('acertijo/actual/respuesta', 'AcertijoController@respuestaActual');
        Route::post('acertijo/actual/responder', 'AcertijoController@responderActual');
        Route::get('acertijo/actual/activar', 'AcertijoController@activarActual');
        Route::get('acertijo/actual/siguiente', 'AcertijoController@actualSiguiente');
        Route::get('acertijo/actual', 'AcertijoController@actual');
        Route::get('grupo/usuarios', 'AcertijoController@grupoUsuarios');
        Route::get('user/acertijo-info', 'AcertijoController@userAcertijoInfo');
        Route::post('user/cambiar-nombre-grupo', 'AcertijoController@changeNameGroup');
       
    });

    Route::get('ranking/general', 'AcertijoController@rankingGeneral');
    Route::get('ranking/acertijo/{index}', 'AcertijoController@rankingAcertijo');
    Route::post('getUsers', 'UserController@getUsers');
    Route::post('getUsersFiltered', 'UserController@getUsersFiltered');
    Route::get('getUser/{id}', 'UserController@getUser');
    Route::post('createUser', 'UserController@createUser');
    Route::get('deleteUser', 'UserController@deleteUser');
    Route::get('createUserField', 'UserController@createUserField');
    Route::post('getAllFields', 'UserController@getAllFields');
    Route::post('getGrupos', 'GrupoController@getGrupos');
    Route::post('getGruposFiltered', 'UserController@getGruposFiltered');

    Route::post('saveUser', 'UserController@saveUser');
    Route::post('deleteAll', 'UserController@softDeleteAll');
    Route::get('deleteColumn', 'UserController@deleteColumn');
    Route::get('getAllFieldsExcel', 'UserController@getAllFieldsExcel');
    Route::post('import', 'UserController@import');
    Route::get('getAllEquipos', 'EquipoController@getAllEquipos');
    Route::post('getExcelRanking', 'UserController@getExcelRanking');
    Route::post('borradoMasivoGrupos', 'GrupoController@borradoMasivoGrupos');
    Route::post('borradoMasivoEquipos', 'EquipoController@borradoMasivoEquipos');

    Route::post('completeEquipo','EquipoController@completeEquipo');
    Route::post('resetProgress','GrupoController@resetProgress');
    Route::get('cambiarNombre','GrupoController@cambiarNombre');






    


    


    




});