<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['logBeforeAfter'])->group(function () {
    Route::get('/', 'GameController@index');
    Route::get('/welcome', 'GameController@welcome');
    Route::get('/acertijo', 'GameController@acertijo');
    Route::get('/ranking', 'GameController@ranking');


    Route::post('/login', 'AuthController@login');

    Route::get('unauthorized',array('as'=>'login',function(){
        return response()->json(array('error'=>'unauthorized'));
    }));

    Route::group(['middleware' => ['auth']], function () {
        Route::get('acertijo/actual/pista/{index}', 'AcertijoController@pistaActual');
        Route::get('acertijo/actual/respuesta', 'AcertijoController@respuestaActual');
        Route::post('acertijo/actual/responder', 'AcertijoController@responderActual');
        Route::get('acertijo/actual/activar', 'AcertijoController@activarActual');//Mandar correos de pistas
        Route::get('acertijo/actual/siguiente', 'AcertijoController@actualSiguiente');
        Route::get('acertijo/actual', 'AcertijoController@actual');
        Route::get('grupo/usuarios', 'AcertijoController@grupoUsuarios');
        Route::get('user/acertijo-info', 'AcertijoController@userAcertijoInfo');
        Route::post('user/cambiar-nombre-grupo', 'AcertijoController@changeNameGroup');
    });

    Route::get('ranking/general', 'AcertijoController@rankingGeneral');
    Route::get('ranking/acertijo/{index}', 'AcertijoController@rankingAcertijo');


    Route::group(['prefix' => 'admin'], function () {
        Route::post('login', ['uses' => 'Auth\LoginController@postLogin', 'as' => 'postlogin']);
        Route::get('grupos/{grupoId}/send-information', 'GrupoController@sendInformation')->name('admin.grupos');
        Route::post('users/import','UserController@import')->name('admin.users');
        Voyager::routes();
    });
});